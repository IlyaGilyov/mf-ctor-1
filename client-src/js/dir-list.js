import debounce from '../../lib/debounce.mjs';
import Dialog from '../../lib/dialog.mjs';

import loadPhotoThumbs from '../../lib/load-photo-thumbs.mjs';

const setupTitleEdit = el => {
	const path = el.dataset.path;
	el.contentEditable = 'true';

	let title = el.textContent.trim();

	let inFocus = false;
	let setTitle = null;
	let restoreTitle = null;

	const save = debounce(async () => {
		const res = await (await fetch('/save-title', {
			headers: {
				'Accept': 'application/json', // eslint-disable-line quote-props
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({ path, title })
		})).json();

		if (null != res.error) {
			if (inFocus) {
				el.blur();
			}

			const msg = new Dialog(res.error, {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());

			el.textContent = restoreTitle;
		} else {
			if (inFocus) {
				setTitle = res.title;
			} else {
				el.textContent = res.title;
			}
		}
	}, 500);

	el.addEventListener('focus', () => {
		inFocus = true;
		restoreTitle = el.textContent.trim();
	});

	el.addEventListener('blur', () => {
		inFocus = false;
		if (null != setTitle) {
			el.textContent = setTitle;
		}
	});

	el.addEventListener('input', () => {
		setTitle = null;
		title = el.textContent.trim();
		save();
	});
};

for (const titleEl of document.querySelectorAll('.js-list-field .js-title')) {
	setupTitleEdit(titleEl);
}

/* Load photo thumbs */

loadPhotoThumbs('.js-photo:not(.stub)', 'preload');
