import {
	// pointerdown,
	pointerup/*,
	pointermove,
	pointercancel*/
} from '../../lib/pointer-event-names.mjs';

import SettingsDialog from '../../lib/settings-dialog.mjs';
import { confirm } from '../../lib/confirm-dialog.mjs';
import Dialog from '../../lib/dialog.mjs';

import ToolsEdit from '../../lib/tools-edit.mjs';

import { edit } from '../../lib/text-edit.mjs';
import debounce from '../../lib/debounce.mjs';

const settingsBtn = document.querySelector('.js-edit-btn');
const delBtn = document.querySelector('.js-del-btn');

const actualizeEntryData = (entryPath = window.entryPath, entryData = window.entryData) => {
	const url = new URL(location.href);
	const currentPath = url.pathname;
	if (currentPath !== entryPath) {
		url.pathname = entryPath;
		window.history.pushState({}, '', url);
	}

	const { title, heading, paragraph } = entryData;

	document.title = title;

	const section = document.querySelector('.js-text-field');

	const h = section.querySelector('h1');
	const pWrap = section.querySelector('div.p-wrap');

	h.innerHTML = heading;
	pWrap.innerHTML = paragraph;
};

const sanitizeData = data => {
	delete data.imageSize;
	delete data.imageURL;

	if (Array.isArray(data.tools)) {
		for (const tool of data.tools) {
			delete tool.svg;
			delete tool.iconSVG;
		}
	}
};

const showSettingsDlg = () => {
	const pathBkp = window.entryPath;
	const dataBkp = JSON.parse(JSON.stringify(window.entryData));

	const dataInp = JSON.parse(JSON.stringify(window.entryData));
	delete dataInp.tools;
	delete dataInp.alloc;

	const dlg = new SettingsDialog(
		window.entryPath,
		dataInp,
		{ /**/ }
	);

	const save = async (path, data, dlg = null) => {
		sanitizeData(data);

		const saveURL = new URL(location.href);
		saveURL.search = '';

		const res = await (await fetch(String(saveURL), {
			headers: {
				'Accept': 'application/json', // eslint-disable-line quote-props
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({ path, data })
		})).json();

		let missingInputErrorMessage = '';

		if (Array.isArray(res.fieldErrors)) {
			for (const { name, message } of res.fieldErrors) {
				const input = null !== dlg ? dlg.getInput(name) : null;
				if (null != input) {
					input.showMessage(message || 'Поле заполнено неправильно', 'error', Infinity);
				} else {
					missingInputErrorMessage += `Сообщение для отсутствующего поля "${name}": ${message}\n`;
				}
			}
		}

		const errorMessage = (res.error || '') + ('' !== missingInputErrorMessage ? `\n${missingInputErrorMessage}` : '');

		if ('' !== errorMessage) {
			res.errorMessage = errorMessage;
		}

		return res;
	};

	let saved = false;

	dlg.on('button:cancel', async () => {
		if (saved) {
			const path = pathBkp;
			const data = dataBkp;

			const res = await save(path, data);

			if (null != res.errorMessage) {
				const msg = new Dialog(res.errorMessage, {
					classes: ['error', 'centered'],
					buttons: [{ name: 'close', caption: 'Ok' }]
				});
				msg.on('button:close', () => msg.close());
			} else {
				window.entryPath = path;
				Object.assign(window.entryData, data);
			}
		}

		dlg.close();
	});

	dlg.on('button:save', async () => {
		dlg.toggleDisableButton('save', true);

		const path = dlg.getPath();
		const data = dlg.getData();

		const res = await save(path, data, dlg);

		if (null != res.errorMessage) {
			const msg = new Dialog(res.errorMessage, {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());
		} else {
			saved = true;

			window.entryPath = path;

			for (const i in data) {
				if (null == data[i]) {
					delete data[i];
				}
			}
			Object.assign(window.entryData, data);

			actualizeEntryData();

			dlg.close();
		}
	});

	dlg.toggleDisableButton('save', true);
};

settingsBtn.addEventListener(pointerup, () => showSettingsDlg());

if (true === window.isNew) {
	showSettingsDlg();
}

delBtn.addEventListener(pointerup, async () => {
	const { confirmed } = await confirm(
		document.createRange().createContextualFragment(
			`Вы действительно хотите удалить страницу <br>"${window.entryPath}"?`
		),
		{ classes: ['delete', 'centered'] }
	);

	if (true === confirmed) {
		const delURL = new URL(location.href);
		delURL.search = '';

		const res = await (await fetch(String(delURL), { method: 'DELETE' })).json();
		if (null != res.error) {
			console.error(res.error);
			const msg = new Dialog('Не удалось удалить страницу', {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());
		} else {
			const redirURL = new URL(location.href);
			const pathChunks = redirURL.pathname.replace(/^\/+|\/+$/, '').split(/\/+/);
			pathChunks.pop();
			redirURL.pathname = `/${pathChunks.join('/')}`;
			redirURL.search = '';
			window.location = String(redirURL);
		}
	}
});

/* Edit text */

{
	// TODO: Check for XSS!!!

	const save = async (propName, html) => {
		window.entryData[propName] = html;

		const saveURL = new URL(location.href);
		saveURL.search = '';

		const res = await (await fetch(String(saveURL), {
			headers: {
				'Accept': 'application/json', // eslint-disable-line quote-props
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({
				data: {
					[propName]: html
				}
			})
		})).json();

		if (null != res.error) {
			const msg = new Dialog(res.error, {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());
		}
	};

	const hEdit = edit(document.querySelector('.js-text-field h1'));
	hEdit.on('change', (html/*, el*/) => save('heading', html));

	const pEdit = edit(document.querySelector('.js-text-field .p-wrap'), { rich: true });
	pEdit.on('change', (html/*, el*/) => save('paragraph', html));
}

/* Load photo */

{
	const label = document.querySelector('.js-img-wrap');

	const fileInput = document.querySelector('.js-load-img');

	fileInput.addEventListener('change', () => {
		const file = fileInput.files[0];

		if (12e6 < file.size) {
			const msg = new Dialog('Размер изображения слишком большой (макс 12МБ)', {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());

			return;
		}

		const ext = (/[^.]+$/.exec(file.name) || ['xx'])[0].toLowerCase();

		if (!['jpg', 'jpeg', 'png', 'bmp', 'tif', 'tiff', 'gif'].includes(ext)) {
			const msg = new Dialog('Неизвестный тип изображения', {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());

			return;
		}

		const reader = new FileReader();
		const onError = error => {
			if (null == error) {
				error = new Error('Unknown error while loading image');
			}

			console.error(error);
			const msg = new Dialog('Не удалось загрузить изображение', {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());
		};
		reader.onload = () => {
			const imgBase64 = String(reader.result).split(',')[1] || null;
			if (null == imgBase64) {
				onError(new Error('Unexpected file reader result'));
				return;
			}

			const imgWrap = document.querySelector('.js-photo-field');
			const img = imgWrap.querySelector('img');

			imgWrap.classList.add('preload');
			img.style.opacity = '0';
			img.style.pointerEvents = 'none';

			label.classList.add('disabled');

			setTimeout(async () => {
				const saveURL = new URL(location.href);
				saveURL.search = '';

				const res = await (await fetch(String(saveURL), {
					headers: {
						'Accept': 'application/json', // eslint-disable-line quote-props
						'Content-Type': 'application/json'
					},
					method: 'POST',
					body: JSON.stringify({ imgBase64 })
				})).json();

				label.classList.remove('disabled');

				if (null != res.error) {
					const msg = new Dialog(res.error, {
						classes: ['error', 'centered'],
						buttons: [{ name: 'close', caption: 'Ok' }]
					});
					msg.on('button:close', () => msg.close());
				} else {
					img.dispatchEvent(new Event('usrreload'));
				}
			}, 500);
		};
		reader.onerror = onError;

		reader.readAsDataURL(file);
	});
}

/* Tools edit */

if (!document.body.classList.contains('reduced')) {
	const { tools, alloc } = JSON.parse(JSON.stringify(window.entryData));
	sanitizeData({ tools, alloc });

	const toolsEdit = new ToolsEdit({
		toolsSrc: window.toolsSrc,
		tools, alloc,

		toolsWrap: document.querySelector('.js-tools-wrap'),
		toolsCnt: document.querySelector('.js-tools-cnt'),
		toolHint: document.querySelector('.js-tool-hint'),
		photoField: document.querySelector('.js-photo-field')
	});

	let toolsRes;
	let allocRes;

	const save = debounce(async () => {
		const saveURL = new URL(location.href);
		saveURL.search = '';

		const data = {};
		if (null != toolsRes) { data.tools = toolsRes; }
		if (null != allocRes) { data.alloc = allocRes; }

		const res = await (await fetch(String(saveURL), {
			headers: {
				'Accept': 'application/json', // eslint-disable-line quote-props
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({ data })
		})).json();

		if (null != res.error) {
			const msg = new Dialog(res.error, {
				classes: ['error', 'centered'],
				buttons: [{ name: 'close', caption: 'Ok' }]
			});
			msg.on('button:close', () => msg.close());
		}
	}, 300);

	toolsEdit.on('change', async (tools, alloc) => {
		toolsRes = tools;
		allocRes = alloc;

		save();
	});

	window.addEventListener('resize', () => toolsEdit.resize());
} else {
	const msg = new Dialog('Редактирование на мобильных устройствах не реализовано', {
		classes: ['error', 'centered'],
		buttons: [{ name: 'close', caption: 'Ok' }]
	});
	msg.on('button:close', () => msg.close());
}
