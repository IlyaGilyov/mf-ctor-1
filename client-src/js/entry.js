import { addCorners } from '../../lib/photo-fld-corners.mjs';
import HlBackground from '../../lib/hl-background.mjs';
import Layout from '../../lib/layout.mjs';
import setupEntry from '../../lib/setup-entry.mjs';


const isTouch = 'ontouchstart' in window || 0 < navigator.maxTouchPoints;
if (isTouch) {
	document.body.classList.add('touch');
}

const entry = setupEntry();

addCorners();

const lo = new Layout({
	toolsBtn: document.querySelector('.js-tools-btn'),
	toolsCloseBtn: document.querySelector('.js-tools-close-btn'),
	toolsField: document.querySelector('.js-tools-field'),
	toolsWrap: document.querySelector('.js-tools-wrap'),
	toolsCnt: document.querySelector('.js-tools-cnt'),
	toolHint: document.querySelector('.js-tool-hint'),
	textField: document.querySelector('.js-text-field'),
	photoField: document.querySelector('.js-photo-field')
});
window.addEventListener('resize', () => lo.resize());
lo.on('change', reduced => {
	if (reduced) {
		document.body.classList.add('reduced');
	} else {
		document.body.classList.remove('reduced');
	}
});
lo.resize();

lo.once('photoready', () => {
	lo.resize();

	if (true !== window.entryEditting) {
		setTimeout(() => {
			if (Array.isArray(entry.start_messages) && 0 !== entry.start_messages.length) {
				lo.showMessages(...entry.start_messages);
			}

			if (Array.isArray(entry.complete_messages) && 0 !== entry.complete_messages.length) {
				lo.once('complete', () => setTimeout(() => lo.showMessages(...entry.complete_messages), 500));
			}
		}, 500);
	}
});
lo.once('toolsready', () => {
	lo.resize();
	lo.setupDrag(entry.alloc.slice());
	lo.setHL(new HlBackground(document.querySelector('.js-hl-bg')));
});

lo.fillText(entry.heading, entry.paragraph);
if (true !== window.entryEditting) {
	lo.setupTools(entry.tools);
}
lo.setupPhoto(entry.imageURL);
