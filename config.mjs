import 'dotenv/config';

import { dirname, join } from 'path';
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));

const {
	APPENV,
	FRONT_PORT,
	FRONT_SECURE_PORT
} = process.env;

const IS_PRODUCTION = 'production' === APPENV;

const ROOT = __dirname;

const ENABLE_SECURE = IS_PRODUCTION;
const FORCE_SECURE = IS_PRODUCTION;
const TLS_CERT_PATH = IS_PRODUCTION ?
	'/etc/letsencrypt/live/sp.marfano.ru/fullchain.pem' :
	join(__dirname, 'dev-cert/dev-localhost-cert.pem');
const TLS_KEY_PATH = IS_PRODUCTION ?
	'/etc/letsencrypt/live/sp.marfano.ru/privkey.pem' :
	join(__dirname, 'dev-cert/dev-localhost-cert.key');

const SESSION_KEY = 'P1zvdDwF';
const APP_KEYS = ['ZXid1JjAk4RvZNxqIAKLwaWjH_0gvDMw'];

const SESSION_MAX_AGE = 1000 * 60 * 60 * 24 * 30;

const CLIENT_SRC_PATH = join(__dirname, 'client-src');
const SRC_CSS_PATH = join(CLIENT_SRC_PATH, 'css');
const SRC_JS_PATH = join(CLIENT_SRC_PATH, 'js');

const JS_LIB_PATH = join(__dirname, 'lib');

const STATIC_PATH = join(__dirname, 'static');
const STATIC_CSS_PATH = join(STATIC_PATH, 'css');
const STATIC_JS_PATH = join(STATIC_PATH, 'js');
const FAVICON_PATH = join(STATIC_PATH, 'favicon', 'favicon.ico');

const STATIC_URL_PATH = '/static'; // should start with slash and should NOT end with slash

const STATIC_MAX_AGE = IS_PRODUCTION ? 1000 * 60 * 60 * 24 * 30 : 0;

const COMPRESS_STATIC = true;
const COMPRESS_MIN_SIZE = 1000;

const TEMPLATE_PATH = join(__dirname, 'templates');
const TEMPLATE_PATH_LOGIN = join(TEMPLATE_PATH, 'login.html');
const TEMPLATE_PATH_ENTRY = join(TEMPLATE_PATH, 'photo-setup.html');
const TEMPLATE_PATH_SVGTOOLS = join(TEMPLATE_PATH, 'svg-tools.html');
const TEMPLATE_PATH_DIRLIST = join(TEMPLATE_PATH, 'dir-list.html');
const TEMPLATE_PATH_ENTRIESLIST = join(TEMPLATE_PATH, 'entries-list.html');

const DATA_PATH = join(__dirname, 'data');
const DELETED_DATA_EXPIRES = IS_PRODUCTION ? 1000 * 60 * 60 * 24 * 30 : 5000;

const DATA_SRC_PATH = join(__dirname, 'data-src');
const DEFAULT_DATA_PATH = join(DATA_SRC_PATH, 'default.data.json');
const DEFAULT_IMAGE_PATH = join(DATA_SRC_PATH, 'default.image.jpg');
const TOOLS_SRC_PATH = join(DATA_SRC_PATH, 'tools.json');

const CERTBOT_WEBROOT_PATH = join(__dirname, 'temp');

const PORT = FRONT_PORT;
const SECURE_PORT = FRONT_SECURE_PORT;

export {
	IS_PRODUCTION,

	ROOT,

	ENABLE_SECURE,
	FORCE_SECURE,
	TLS_CERT_PATH,
	TLS_KEY_PATH,

	SESSION_KEY,
	APP_KEYS,

	SESSION_MAX_AGE,

	CLIENT_SRC_PATH,
	SRC_CSS_PATH,
	SRC_JS_PATH,
	JS_LIB_PATH,
	FAVICON_PATH,

	STATIC_PATH,
	STATIC_CSS_PATH,
	STATIC_JS_PATH,

	STATIC_URL_PATH,

	STATIC_MAX_AGE,

	COMPRESS_STATIC,
	COMPRESS_MIN_SIZE,

	TEMPLATE_PATH,
	TEMPLATE_PATH_LOGIN,
	TEMPLATE_PATH_ENTRY,
	TEMPLATE_PATH_SVGTOOLS,
	TEMPLATE_PATH_DIRLIST,
	TEMPLATE_PATH_ENTRIESLIST,

	DATA_PATH,
	DELETED_DATA_EXPIRES,

	DATA_SRC_PATH,
	DEFAULT_DATA_PATH,
	DEFAULT_IMAGE_PATH,
	TOOLS_SRC_PATH,

	CERTBOT_WEBROOT_PATH,

	PORT,
	SECURE_PORT
};
