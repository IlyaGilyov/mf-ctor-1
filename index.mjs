import {
	IS_PRODUCTION,

	ENABLE_SECURE,
	FORCE_SECURE,
	TLS_CERT_PATH,
	TLS_KEY_PATH,

	SESSION_KEY,
	APP_KEYS,

	SESSION_MAX_AGE,

	STATIC_PATH,
	STATIC_URL_PATH,
	STATIC_MAX_AGE,
	COMPRESS_STATIC,

	TEMPLATE_PATH_LOGIN,
	TEMPLATE_PATH_ENTRY,
	TEMPLATE_PATH_SVGTOOLS,
	TEMPLATE_PATH_DIRLIST,
	TEMPLATE_PATH_ENTRIESLIST,

	FAVICON_PATH,

	DATA_PATH,
	DEFAULT_DATA_PATH,
	DEFAULT_IMAGE_PATH,
	TOOLS_SRC_PATH,

	CERTBOT_WEBROOT_PATH,

	PORT,
	SECURE_PORT
} from './config.mjs';

import { join } from 'path';
import { access, readFile, mkdir, writeFile, rename, copyFile, rm } from 'fs/promises';
import { createReadStream } from 'fs';
import { createHash } from 'crypto';
import https from 'https';

import Koa from 'koa';
import Session from 'koa-session';
import send from 'koa-send';
import logger from 'koa-logger';
import koaBody from 'koa-body';
import favicon from 'koa-favicon';

import mustache from 'mustache';

import { nanoid } from 'nanoid';

import jimp from 'jimp';

import calipers from 'calipers';
const { measure: getImageSize } = calipers('jpeg');

import chokidar from 'chokidar';
import DataTree from './lib/data-tree.mjs';
import debounce from './lib/debounce.mjs';

import denyReq from './lib/deny-req.mjs';
import readTemplate from './lib/read-template.mjs';
import clearData from './lib/clear-data.mjs';


const app = new Koa();

const seed = nanoid(10);
console.info('seed:', seed);

/*
	////////////////////////////////
	LOGGER MW
	////////////////////////////////
*/

app.use(logger());

/*
	////////////////////////////////
	DENY COMMON REQUESTS
	////////////////////////////////
*/

app.use(denyReq.mw());

/*
	////////////////////////////////
	FORCE HTTPS
	////////////////////////////////
*/

if (ENABLE_SECURE && FORCE_SECURE) {
	app.use(async (ctx, next) => {
		if (!ctx.secure) {
			const urlStr = `https://${ctx.get('host')}${ctx.originalUrl}`;
			let secureURL;
			try {
				secureURL = new URL(urlStr);
			} catch (err) {
				console.error(new Error(`Failed to parse url string "${urlStr}"`));
				console.error(err);
				ctx.throw(500);

				return;
			}

			if (443 !== SECURE_PORT) {
				secureURL.port = SECURE_PORT;
			}

			ctx.status = IS_PRODUCTION ? 301 : 302;
			ctx.redirect(String(secureURL));

			return;
		}

		await next();
	});
}

/*
	////////////////////////////////
	BODY PARSER MW
	////////////////////////////////
*/

app.use(koaBody({
	multipart: true,
	jsonLimit: '20mb'
}));

/*
	////////////////////////////////
	AUTH (WIP)
	////////////////////////////////
*/

{
	const TMP_USER_NAME = 'admin'; // WIP
	const TMP_USER_PASSWORD_SHA256 = 'df7688709dee5e7efa731a0db8ad3fc493593de943127b47c251cc216028f6cd'; // WIP

	const session = Session({
		key: SESSION_KEY,
		maxAge: SESSION_MAX_AGE,
		renew: true,
		secure: ENABLE_SECURE,
		signed: IS_PRODUCTION
	}, app);

	app.keys = APP_KEYS;
	app.use(session);

	app.use(async (ctx, next) => {
		const { path } = ctx;

		const reqLogIn = '/login' === path || '/login/' === path;
		const reqLogOut = '/logout' === path || '/logout/' === path;

		if (reqLogIn || reqLogOut) {
			const loginTemplate = await readTemplate(TEMPLATE_PATH_LOGIN);

			const username = ctx.session && ctx.session.user && ctx.session.user.name || null;

			let data = { seed };
			if (TMP_USER_NAME === username) { // WIP
				data = {
					...data,
					success: true,
					form_disabled: true,
					can_logout: true,
					message: `Вход выполнен (${username})`
				};
			}

			if (reqLogIn && 'POST' === ctx.method) {
				data = {
					...data,
					error: true,
					message: 'Вход не выполнен'
				};

				const { body: loginData } = ctx.request;

				if (null != loginData) {
					const { seed: formSeed, username, password } = loginData;
					if (
						seed === formSeed &&
						username === TMP_USER_NAME // WIP
					) {
						try {
							if (
								createHash('sha256').update(password).digest('hex') ===
								TMP_USER_PASSWORD_SHA256 // WIP
							) {
								data = {
									...data,
									error: false,
									success: true,
									form_disabled: true,
									can_logout: true,
									message: `Вход выполнен (${username})`
								};

								ctx.session.user = {
									name: TMP_USER_NAME,
									canEdit: true
								};
							}
						} catch (err) {
							console.error(err);
						}
					}
				}
			} else if (reqLogOut && null !== username) {
				data = { seed };
				ctx.session.user = null;
			}

			ctx.type = 'text/html; charset=utf-8';
			ctx.body = mustache.render(loginTemplate, data);

			return;
		}

		await next();
	});
}

/*
	////////////////////////////////
	STATIC
	////////////////////////////////
*/

{
	const staticPathRE = new RegExp(`^${STATIC_URL_PATH}\\b`, 'i');

	app.use(async (ctx, next) => {
		if (staticPathRE.test(ctx.path)) {
			try {
				await send(ctx, ctx.path.replace(staticPathRE, '') || '/', {
					root: STATIC_PATH,
					maxage: STATIC_MAX_AGE,
					immutable: IS_PRODUCTION,
					gzip: COMPRESS_STATIC,
					brotli: COMPRESS_STATIC
				});
			} catch (err) {
				console.error(err);
				ctx.throw(404);
			}

			return;
		}

		await next();
	});
}

/*
	////////////////////////////////
	FAVICON MW
	////////////////////////////////
*/

app.use(favicon(FAVICON_PATH));

/*
	////////////////////////////////
	CERTBOT
	////////////////////////////////
*/

{
	const certbotReqStr = '/.well-known/';

	app.use(async (ctx, next) => {
		if (0 === ctx.path.indexOf(certbotReqStr)) {
			try {
				await send(ctx, ctx.path, {
					root: CERTBOT_WEBROOT_PATH,
					hidden: true,
					maxage: 0,
					immutable: false
				});
			} catch (err) {
				console.error(err);

				ctx.throw(404);
			}

			return;
		}

		await next();
	});
}

/*
	////////////////////////////////
	MAIN
	////////////////////////////////
*/

(async () => {
	try {
		await access(DATA_PATH);
	} catch {
		console.log('Data directory does not exist. Creating...');
		await mkdir(DATA_PATH);
	}

	const IMAGE_CHUNK_RE = /\.jpe?g$/;

	let toolsSrcJSONCache;

	let dataTree;

	const updateDataTree = async () => {
		if (undefined === dataTree) {
			console.log('Initializing data tree');
			const st = Date.now();
			dataTree = new DataTree(DATA_PATH);
			console.log('Data tree initialized', Date.now() - st, 'ms');
		} else {
			console.log('Updating data tree');
			const st = Date.now();
			dataTree.update();
			console.log('Data tree updated', Date.now() - st, 'ms');
		}
	};

	const updateDataTreeDebounced = debounce(updateDataTree, 1000);

	chokidar.watch([DATA_PATH])
		.on('add', updateDataTreeDebounced)
		.on('unlink', updateDataTreeDebounced)
		.on('addDir', updateDataTreeDebounced)
		.on('unlinkDir', updateDataTreeDebounced);

	app.use(async (ctx/*, next*/) => {
		const {
			path,
			session
		} = ctx;

		const user = null != session ? session.user || {} : {};
		ctx.user = user;

		const query = 'object' === typeof ctx.query ? { ...ctx.query } : {};

		const pathLC = path.toLowerCase().replace(/\/+$/g, '');
		const pathChunks = pathLC.replace(/^\/+/g, '').split(/\/+/).filter(str => '' !== str);

		if ('/save-title' === pathLC) {
			if ('POST' !== ctx.method) {
				ctx.throw(405);
				return;
			}

			if (true !== user.canEdit) {
				ctx.throw(404);
				return;
			}

			const bodyData = ctx.request.body;
			const { path, title } = bodyData;

			const entry = dataTree.getEntry(path);

			if (null == entry) {
				ctx.throw(404);
				return;
			}

			const titleTrimmed = title.trim();

			const titleFilePath = join(DATA_PATH, path, '.title');

			let titleFileExists;
			try {
				await access(titleFilePath);
				titleFileExists = true;
			} catch {
				titleFileExists = false;
			}

			let titleImpl = titleTrimmed;
			let error = null;

			if ('' === titleTrimmed) {
				if (titleFileExists) {
					try {
						await rm(titleFilePath);
					} catch (err) {
						console.error(err);
					}
				}

				titleImpl = path;
			} else {
				try {
					await writeFile(titleFilePath, titleImpl);
				} catch (err) {
					console.error(err);
					error = 'Не удалось сохранить заголовок';
				}
			}

			const resData = {};

			if (null != error) {
				resData.error = error;
			} else {
				resData.title = titleImpl;
				updateDataTree();
			}

			ctx.type = 'application/json; charset=utf-8';
			ctx.body = JSON.stringify(resData);

			return;
		}

		if (0 === pathChunks.length) {
			const THUMBS_COUNT = 4;

			const dirs = dataTree.getDirs('/', THUMBS_COUNT);

			if (0 !== dirs.length) {
				const items = Array(dirs.length);
				for (let i = 0; i < items.length; ++i) {
					let srcEntry = dirs[i];

					if (THUMBS_COUNT > srcEntry.entries.length) {
						const entries = Array(THUMBS_COUNT);
						for (let i = 0, srcEntries = srcEntry.entries; i < THUMBS_COUNT; ++i) {
							const srcEntry = srcEntries[i];
							entries[i] = null != srcEntry ? srcEntry : { is_stub: true };
						}

						srcEntry = { ...srcEntry, entries };
					}

					items[i] = {
						...srcEntry,
						title: srcEntry.title || srcEntry.path
					};
				}

				const templateData = {
					seed,
					title: 'Список директорий',
					is_root: true,
					logo_tagname: 'i',
					can_edit: true === user.canEdit,
					items
				};

				ctx.type = 'text/html; charset=utf-8';
				ctx.body = mustache.render(await readTemplate(TEMPLATE_PATH_DIRLIST), templateData);
				return;
			}

			ctx.throw(404);
			return;
		}

		if ('/stub.image.jpg' === path) {
			ctx.type = 'image/jpeg';
			ctx.body = createReadStream(DEFAULT_IMAGE_PATH);

			return;
		}

		const lastChunk = pathChunks.pop();
		let entryBasename = lastChunk;
		let reqImage = false;
		if (IMAGE_CHUNK_RE.test(lastChunk)) {
			entryBasename = lastChunk.replace(IMAGE_CHUNK_RE, '');
			reqImage = true;
		}

		if (!reqImage) {
			const entry = dataTree.getEntry(pathLC);

			if (null != entry && entry.isDir) {
				const templateData = {
					seed,
					title: entry.title || entry.path,
					logo_tagname: 'a',
					can_edit: true === user.canEdit,
					items: entry.entries
				};

				ctx.type = 'text/html; charset=utf-8';
				ctx.body = mustache.render(await readTemplate(TEMPLATE_PATH_ENTRIESLIST), templateData);

				return;
			}
		}

		if (!/^\w+$/.test(entryBasename)) {
			ctx.throw(404);
			return;
		}

		const dataFileName = `${entryBasename}.data.json`;
		const imageFileName = `${entryBasename}.image.jpg`;

		const dataFilePath = join(DATA_PATH, ...pathChunks, dataFileName);
		const imageFilePath = join(DATA_PATH, ...pathChunks, imageFileName);
		let entryExists = true;
		try {
			await access(dataFilePath);
			await access(imageFilePath);
		} catch {
			entryExists = false;
		}

		if (!entryExists && !user.canEdit) {
			ctx.throw(404);
			return;
		}

		if ('DELETE' === ctx.method) {
			if (!user.canEdit) {
				ctx.throw(405);
				return;
			}

			let error = null;

			try {
				await clearData();
				await rename(dataFilePath, `${dataFilePath}.bak`);
				await rename(imageFilePath, `${imageFilePath}.bak`);
				await updateDataTree();
			} catch (err) {
				console.error(err);
				error = 'Failed to delete entry';
			}

			ctx.type = 'application/json; charset=utf-8';
			ctx.body = JSON.stringify({ error });

			return;
		}

		if (reqImage) {
			const imageFilePathImpl = entryExists ? imageFilePath : DEFAULT_IMAGE_PATH;

			ctx.type = 'image/jpeg';
			ctx.body = createReadStream(imageFilePathImpl);
			return;
		}

		const entryPath = `/${0 === pathChunks.length ? '' : pathChunks.join('/') + '/'}${entryBasename}`;

		if ('POST' === ctx.method) {
			if (!user.canEdit) {
				ctx.throw(405);
				return;
			}

			const bodyData = ctx.request.body;
			const { path } = bodyData;
			let { data, imgBase64 } = bodyData;

			if (!entryExists) {
				if (null == data) {
					data = JSON.parse(await readTemplate(DEFAULT_DATA_PATH));
				}

				if (null == imgBase64) {
					imgBase64 = await readFile(DEFAULT_IMAGE_PATH, 'base64');
				}
			}

			const pathLC = 'string' === typeof path ? path.toLowerCase() : null;

			let entryData = {};
			if (null != data) {
				if (entryExists) {
					entryData = JSON.parse(String(await readFile(dataFilePath)));
				} else {
					entryData = JSON.parse(await readTemplate(DEFAULT_DATA_PATH));
				}
			}

			let dataFilePathImpl = dataFilePath;
			let imageFilePathImpl = imageFilePath;

			if (null !== pathLC && pathLC !== entryPath) {
				if (!/^\/[\w\/]+$/.test(pathLC) || pathLC.lastIndexOf('/') === pathLC.length - 1) {
					ctx.type = 'application/json; charset=utf-8';
					ctx.body = JSON.stringify({
						fieldErrors: [
							{ name: 'path', message: 'Неправильный адрес страницы' }
						]
					});

					return;
				}

				let exists;
				try {
					await access(join(DATA_PATH, pathLC));
					exists = true;
				} catch {
					exists = false;
				}

				if (!exists) {
					try {
						await access(join(DATA_PATH, `${pathLC}.data.json`));
						exists = true;
					} catch {
						exists = false;
					}
				}

				if (exists) {
					ctx.type = 'application/json; charset=utf-8';
					ctx.body = JSON.stringify({
						fieldErrors: [
							{ name: 'path', message: 'Такая страница или директория уже есть' }
						]
					});

					return;
				}

				const newDataDir = join(DATA_PATH, pathLC.replace(/\/[^\/]+$/, ''));
				let newDataDirExists;
				try {
					await access(newDataDir);
					newDataDirExists = true;
				} catch {
					newDataDirExists = false;
				}

				if (!newDataDirExists) {
					await mkdir(newDataDir, { recursive: true });
				}

				dataFilePathImpl = join(DATA_PATH, `${pathLC}.data.json`);
				imageFilePathImpl = join(DATA_PATH, `${pathLC}.image.jpg`);

				let oldDataExists;
				try {
					await access(dataFilePath);
					oldDataExists = true;
				} catch {
					oldDataExists = false;
				}

				if (oldDataExists) {
					if (null == data) {
						try {
							await copyFile(dataFilePath, dataFilePathImpl);
						} catch (err) {
							console.error(err);

							ctx.type = 'application/json; charset=utf-8';
							ctx.body = JSON.stringify({
								error: 'Не удалось сохранить страницу'
							});

							return;
						}
					}

					if (null == imgBase64) {
						try {
							await copyFile(imageFilePath, imageFilePathImpl);
						} catch (err) {
							console.error(err);

							ctx.type = 'application/json; charset=utf-8';
							ctx.body = JSON.stringify({
								error: 'Не удалось сохранить страницу'
							});

							return;
						}
					}

					try {
						await clearData();
						await rename(dataFilePath, `${dataFilePath}.bak`);
						await rename(imageFilePath, `${imageFilePath}.bak`);
					} catch (err) {
						console.error(err);

						ctx.type = 'application/json; charset=utf-8';
						ctx.body = JSON.stringify({
							error: 'Не удалось сохранить страницу'
						});

						return;
					}
				}
			}

			if (null != data) {
				const dataImpl = {
					...entryData,
					...data
				};

				if (!entryExists) {
					const defaultData = JSON.parse(await readTemplate(DEFAULT_DATA_PATH));
					let invalidPropName = null;
					for (const propName in defaultData) {
						if (!dataImpl.hasOwnProperty(propName)) {
							invalidPropName = propName;
							break;
						}

						const defaultValue = defaultData[propName];
						const value = dataImpl[propName];

						if (typeof value !== typeof defaultValue) {
							invalidPropName = propName;
							break;
						}

						if (Array.isArray(defaultValue) && !Array.isArray(value)) {
							invalidPropName = propName;
							break;
						}
					}

					if (null !== invalidPropName) {
						ctx.type = 'application/json; charset=utf-8';
						ctx.body = JSON.stringify({
							error: `Отсутствует или некорректное поле "${invalidPropName}"`
						});

						return;
					}
				} else {
					if (!dataImpl.title) {
						ctx.type = 'application/json; charset=utf-8';
						ctx.body = JSON.stringify({
							fieldErrors: [
								{ name: 'title', message: 'Необходимо указать заголовок' }
							]
						});

						return;
					}
				}

				try {
					await writeFile(dataFilePathImpl, JSON.stringify(dataImpl));
				} catch (err) {
					console.error(err);
				}
			}

			if (null != imgBase64) {
				let jimpImg;
				try {
					jimpImg = await jimp.read(Buffer.from(imgBase64, 'base64'));
				} catch (err) {
					console.error(err);

					ctx.type = 'application/json; charset=utf-8';
					ctx.body = JSON.stringify({
						error: 'Не удалось распонать изображение'
					});

					return;
				}

				const { bitmap: { height } } = jimpImg;

				if (height > 2100) {
					jimpImg.resize(jimp.AUTO, 2100);
				}

				jimpImg.quality(82);

				try {
					await jimpImg.writeAsync(imageFilePathImpl);
				} catch (err) {
					console.error(err);

					ctx.type = 'application/json; charset=utf-8';
					ctx.body = JSON.stringify({
						error: 'Не удалось сохранить изображение'
					});

					return;
				}
			}

			ctx.type = 'application/json; charset=utf-8';
			ctx.body = JSON.stringify({ success: true });

			return;
		}

		let templateData = {
			seed,
			entry_path: entryPath,
			is_new: !entryExists,
			svg_tools: await readTemplate(TEMPLATE_PATH_SVGTOOLS)
		};

		if (true === user.canEdit) {
			templateData.can_edit = true;
		}

		if (entryExists) {
			const entryData = JSON.parse(String(await readFile(dataFilePath)));

			const imageURL = `${entryPath}.jpg?${nanoid(8)}`;
			let { imageSize } = entryData;
			if (null == imageSize) {
				imageSize = (await getImageSize(imageFilePath)).pages[0];
				entryData.imageSize = imageSize;
				await writeFile(
					dataFilePath,
					IS_PRODUCTION ?
						JSON.stringify(entryData) :
						JSON.stringify(entryData, null, 2)
				);
			}

			templateData = {
				...templateData,
				title: entryData.title,
				image_url: imageURL,
				image_width: imageSize.width,
				image_height: imageSize.height,
				entry_data_json: JSON.stringify(entryData)
			};

			if (true === user.canEdit && query.hasOwnProperty('edit')) {
				let toolsSrcJSON = toolsSrcJSONCache;
				if (undefined === toolsSrcJSON) {
					toolsSrcJSON = JSON.stringify(
						JSON.parse(
							await readTemplate(TOOLS_SRC_PATH)
						)
					);

					if (IS_PRODUCTION) {
						toolsSrcJSONCache = toolsSrcJSON;
					}
				}

				templateData.tools_src_json = toolsSrcJSON;
				templateData.edit = true;
			}
		} else {
			if (!query.hasOwnProperty('edit')) {
				ctx.redirect(`${path.toLowerCase()}?edit`);
				return;
			}

			const entryDataJSON = await readTemplate(DEFAULT_DATA_PATH);

			let toolsSrcJSON = toolsSrcJSONCache;
			if (undefined === toolsSrcJSON) {
				toolsSrcJSON = JSON.stringify(
					JSON.parse(
						await readTemplate(TOOLS_SRC_PATH)
					)
				);

				if (IS_PRODUCTION) {
					toolsSrcJSONCache = toolsSrcJSON;
				}
			}

			templateData = {
				...templateData,
				title: 'Новый',
				image_url: `${entryPath}.jpg?${nanoid(8)}`,
				image_width: 600,
				image_height: 800,
				tools_src_json: toolsSrcJSON,
				edit: true,
				entry_data_json: entryDataJSON
			};
		}

		ctx.type = 'text/html; charset=utf-8';
		ctx.body = mustache.render(await readTemplate(TEMPLATE_PATH_ENTRY), templateData);
	});
})();


/*
	////////////////////////////////
	START SERVER
	////////////////////////////////
*/

app.listen(PORT, () => console.log(`Server is listening on port ${PORT}`));

if (ENABLE_SECURE) {
	(async () =>
		https.createServer(
			{
				cert: await readFile(TLS_CERT_PATH),
				key: await readFile(TLS_KEY_PATH)
			},
			app.callback()
		).listen(
			SECURE_PORT,
			() => console.log(`Server is listening on port ${SECURE_PORT} (secure)`)
		)
	)();
}
