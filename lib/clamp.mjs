const clamp = (n, min = -Infinity, max = Infinity) => {
	if (n < min) {
		return min;
	}
	if (n > max) {
		return max;
	}
	return n;
};

export default clamp;
