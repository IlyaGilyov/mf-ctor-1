import {
	DATA_PATH,
	DELETED_DATA_EXPIRES
} from '../config.mjs';

import { join, extname } from 'path';
import { readdir, stat, rm } from 'fs/promises';

const clearData = async (dir = DATA_PATH) => {
	const items = await readdir(dir);

	if (0 === items.length) {
		await rm(dir, { recursive: true });
	} else {
		for (const itemName of items) {
			const itemPath = join(dir, itemName);
			const itemStat = await stat(itemPath);
			if (itemStat.isDirectory()) {
				await clearData(itemPath);
			} else if (
				itemStat.isFile() &&
				'.bak' === extname(itemName) &&
				Date.now() - itemStat.birthtimeMs >= DELETED_DATA_EXPIRES
			) {
				await rm(itemPath);
			}
		}
	}
};

export default clearData;
