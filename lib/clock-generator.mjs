const global = window;

const now = performance.now.bind(performance);

const DEFAULT_BASE_FREQUENCY = 60;

const defaultOptions = {
	baseFrequency: DEFAULT_BASE_FREQUENCY,
	frequency: 30,
	start: false,
	autoRun: false,
	forceTimeout: false,
	execFirst: false,
	forceContext: null,
	requestAnimationFrame: null,
	cancelAnimationFrame: null
};

class ClockGenerator {
	constructor(options = {}) {
		this._options = Object.assign({}, defaultOptions, options);
		this.setFrequency(this._options.frequency);
		if (null != this._options.forceContext) {
			this._forceContext = this._options.forceContext;
		}
		this._listeners = undefined !== this._forceContext ? new Map() : new Set();
		this._lowCycles = 0;
		this._highCycles = 0;
		this._running = false;
		this._forceTimeout = true === this._options.forceTimeout;
		if (true === this._options.execFirst) {
			this._cycle = currentTime => {
				this._exec(currentTime);
				this._queue();
			};
		} else {
			this._cycle = currentTime => {
				this._queue();
				this._exec(currentTime);
			};
		}

		if (true === this._options.start) {
			if (true === this._options.autoRun) {
				throw new Error('Start and autorun features can not be enabled simultaneously');
			}

			this.start();
		}
	}
	_resetTimers() {
		if (undefined !== this._timers) {
			this._clearTimers();
		}
		this._timers = {};
		if ('function' === typeof this._options.requestAnimationFrame && 'function' === typeof this._options.cancelAnimationFrame) {
			this._timers.requestFrame = this._options.requestAnimationFrame;
			this._timers.cancelFrame = this._options.cancelAnimationFrame;
		} else if (true === this._forceTimeout || 'function' !== typeof global.requestAnimationFrame || 'function' !== typeof global.cancelAnimationFrame) {
			const baseFrequency = true === this._forceTimeout ? this._frequency : this._options.baseFrequency;
			this._timers.requestFrame = callback => setTimeout(() => callback(now()), 1000 / baseFrequency);
			this._timers.cancelFrame = timer => clearTimeout(timer);
		} else {
			this._timers.requestFrame = global.requestAnimationFrame.bind(global);
			this._timers.cancelFrame = global.cancelAnimationFrame.bind(global);
		}
	}
	_activateTimers(immediate = false) {
		this._resetTimers();
		// clearTimeout(this._activateTimer);
		if (true === immediate) {
			this._cycle(now());
		} else {
			// this._activateTimer = setTimeout(() => this._cycle(now()), 1);
			this._queue();
		}
	}
	_clearTimers() {
		// clearTimeout(this._activateTimer);
		this._timers.cancelFrame(this._timer);
		delete this._timers;
	}
	_getBaseFrequency(currentTime) {
		let baseFrequency = 'number' === typeof this._baseFrequency ? this._baseFrequency : this._options.baseFrequency;
		if ('number' === typeof this._previousTime) {
			baseFrequency = Math.round(1000 / (currentTime - this._previousTime) / 5) * 5;
		}
		this._previousTime = currentTime;
		this._baseFrequency = baseFrequency;
		return baseFrequency;
	}
	_exec(currentTime) {
		if (
			this._running &&
			(true === this._forceTimeout || Infinity === this._frequency || 0 === Math.floor(this._lowCycles++ % (this._getBaseFrequency(currentTime) / this._frequency)))
		) {
			const highCycles = this._highCycles++;
			for (const listener of this._listeners.values()) {
				listener(currentTime, highCycles);
			}
		}
	}
	_queue() {
		if (undefined !== this._timers) {
			this._timer = this._timers.requestFrame(this._cycle);
		}
	}
	start() {
		if (this._running) {
			return;
		}
		this._running = true;
		if (undefined === this._timers) {
			this._activateTimers();
		}
	}
	stop() {
		this._lowCycles = 0;
		this._highCycles = 0;
		this._running = false;
		if (undefined !== this._timers) {
			this._clearTimers();
		}
	}
	pause() {
		this._running = false;
	}
	runCycle(currentTime = now()) {
		for (const listener of this._listeners.values()) {
			listener(currentTime, this._highCycles);
		}
	}
	runCycles(count = Infinity) {
		if (this._running) {
			return Promise.reject(new Error('Already running'));
		}

		return new Promise(resolve => {
			const listener = () => {
				if (--count <= 0) {
					this.removeListener(listener);
					this.stop();
					resolve();
				}
			};
			this.addListener(listener);
			this.start();
		});
	}
	forceTimeout(state = true) {
		if (state !== this._forceTimeout) {
			this._forceTimeout = state;
			if (undefined !== this._timers) {
				this._activateTimers();
			}
		}
	}
	addListener(listener, singleShot = false) {
		if ('function' !== typeof listener) {
			throw new TypeError('Listener is not a function');
		}

		const listeners = this._listeners;

		if (undefined !== this._forceContext) {
			listeners.set(listener, listener.bind(this._forceContext));
		} else {
			listeners.add(listener);
		}

		if (true === singleShot) {
			const janitor = () => {
				this.removeListener(listener);
				this.removeListener(janitor);
			};
			if (listeners instanceof Map) {
				listeners.set(janitor, janitor);
			} else {
				listeners.add(janitor);
			}
		}

		if (true === this._options.autoRun) {
			this.start();
		}
	}
	removeListener(listener) {
		const res = this._listeners.delete(listener);

		if (true === this._options.autoRun && this._listeners.size === 0) {
			this.stop();
		}

		return res;
	}
	hasListener(listener) {
		return this._listeners.has(listener);
	}
	setFrequency(value) {
		if (Infinity === value && true !== this._forceTimeout) {
			this._frequency = Infinity;
		} else {
			this._frequency = Math.max(1, Math.min(value, 60)) || defaultOptions.frequency;
		}
		if (true === this._forceTimeout && undefined !== this._timers) {
			if (this._running) {
				this._activateTimers(true);
			} else {
				this._resetTimers();
			}
		}
	}
	getFrequency() {
		return this._frequency;
	}
	getLowCycles() {
		return this._lowCycles;
	}
	getHighCycles() {
		return this._highCycles;
	}
	isAutoRun() {
		return true === this._options.autoRun;
	}
	isRunning() {
		return this._running;
	}
}

export default ClockGenerator;
