import Dialog from './dialog.mjs';

const defaultOptions = {
	buttons: [
		{ name: 'decline', caption: 'Нет' },
		{ name: 'confirm', caption: 'Да' }
	],
	closeBtn: false
};

class ConfirmDialog extends Dialog {
	constructor(message, options = {}) {
		super(message, {
			...defaultOptions,
			...options
		});

		this.confirmResult = 'none';

		this.on('button:confirm', () => this._onBtnConfirm());
		this.on('button:decline', () => this._onBtnDecline());
	}

	_onBtnConfirm() {
		this.confirmResult = 'confirm';
		this.emit('confirm');
		this.close();
	}

	_onBtnDecline() {
		this.confirmResult = 'decline';
		this.emit('decline');
		this.close();
	}
}

const confirm = (message, options) => new Promise((resolve/*, reject*/) => {
	const dialog = new ConfirmDialog(message, options);
	dialog.once('close', () => resolve({
		result: dialog.confirmResult,
		confirmed: 'confirm' === dialog.confirmResult,
		dialog
	}));
});

export { confirm };
export default ConfirmDialog;
