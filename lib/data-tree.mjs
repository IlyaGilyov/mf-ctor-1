import EventEmitter from './events.mjs';

import { join } from 'path';
import { readdir/*, access*/, readFile/*, mkdir, writeFile, rename, copyFile*/ } from 'fs/promises';

const getTreeRec = async (dir, path, entryCb = null) => {
	const entries = [];

	const entryPath = '/' === path ? path : path.length - 1 === path.lastIndexOf('/') ? path.slice(0, -1) : path;

	const tree = {
		path: entryPath,
		entries,
		isDir: true
	};

	const titleFilePath = join(dir, '.title');

	try {
		const title = String(await readFile(titleFilePath));
		tree.title = title;
	} catch {
		/* noop */
	}

	if ('function' === typeof entryCb) {
		entryCb(entryPath, tree);
	}

	for (const dirent of await readdir(dir, { withFileTypes: true })) {
		const { name: fileName } = dirent;
		if (dirent.isDirectory()) {
			const subTree = await getTreeRec(join(dir, fileName), `${path}${fileName}/`, entryCb);
			if (0 !== subTree.entries.length) {
				entries.push(subTree);
			}
		} else if (dirent.isFile()) {
			if (fileName.length - 10 === fileName.lastIndexOf('.data.json')) {
				const name = fileName.slice(0, -10);
				const entryPath = `${path}${name}`;
				const imagePath = `${path}${name}.jpg`;

				const entry = {
					name,
					path: entryPath,
					image_path: imagePath
				};

				if ('function' === typeof entryCb) {
					entryCb(entryPath, entry);
				}

				entries.push(entry);
			}
		}
	}

	return tree;
};

class DataTree extends EventEmitter {
	constructor(rootPath) {
		super();

		this._rootPath = rootPath;

		this._tree = {};
		this._map = new Map();

		this._updatePromise = Promise.resolve();
		this.update();
	}

	async _updateImpl() {
		const map = new Map();

		const tree = await getTreeRec(
			this._rootPath, '/',
			(path, entry) => map.set(path, entry)
		);

		this._tree = tree;
		this._map = map;

		return tree;
	}

	update() {
		this._updatePromise = this._updatePromise.then(() => this._updateImpl());
		return this._updatePromise;
	}

	getEntry(path) {
		return this._map.get(path) || null;
	}

	getDirs(path, limitEntries = -1) {
		const dirEntry = this._map.get(path);
		if (undefined === dirEntry || true !== dirEntry.isDir) {
			throw new ReferenceError(`Path "${path}" not found or is not directory`);
		}

		const res = [];

		for (const entry of dirEntry.entries) {
			if (true === entry.isDir) {
				if (0 <= limitEntries) {
					res.push({
						...entry,
						entries: entry.entries.slice(0, limitEntries)
					});
				} else {
					res.push(entry);
				}
			}
		}

		return res;
	}

	getEntries(path) {
		const entry = this._map.get(path);
		if (undefined === entry || true !== entry.isDir) {
			throw new ReferenceError(`Path "${path}" not found or is not directory`);
		}

		// ...
	}
}

export default DataTree;
