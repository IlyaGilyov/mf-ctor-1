import { readFileSync } from 'fs';
import { join } from 'path';

import { ROOT } from '../config.mjs';

const res404 = { status: 404 };

const entries = [
	{ str: 'robots.txt', res: { body: readFileSync(join(ROOT, 'client-src', 'robots.txt')) } },
	{
		str: 'sitemap.xml',
		res: res404
	}
];
const strEntries = new Map();
const reEntries = new Map();
for (let i = 0; i < entries.length; ++i) {
	const { str, re, res } = entries[i];
	if (Array.isArray(str)) {
		for (let i = 0; i < str.length; ++i) {
			strEntries.set(str[i].toLowerCase(), res);
		}
	} else if ('string' === typeof str) {
		strEntries.set(str[i].toLowerCase(), res);
	}

	if (re instanceof RegExp) {
		reEntries.set(re, res);
	}
}

const denyReq = ctx => {
	let path = ctx.path.toLowerCase();
	for (; 0 === path.indexOf('/');) {
		path = path.slice(1);
	}

	for (const [str, res] of strEntries.entries()) {
		if (str === path) {
			return res;
		}
	}

	for (const [re, res] of reEntries.entries()) {
		if (re.test(path)) {
			return res;
		}
	}

	return null;
};

const mw = () => async (ctx, next) => {
	const denyRes = denyReq(ctx);
	if (null !== denyRes) {
		const { status, type, body } = denyRes;

		if (undefined !== status) {
			if (status >= 400) {
				ctx.throw(status);
				return;
			}

			ctx.status = status;
		}

		ctx.type = type || 'text/plain';
		ctx.body = body || '';

		return;
	}

	await next();
};

denyReq.mw = mw;

export default denyReq;
