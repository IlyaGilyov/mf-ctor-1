import {
	// pointerdown,
	pointerup/*,
	pointermove,
	pointercancel*/
} from './pointer-event-names.mjs';

import DialogTextInput from './dialog-text-input.mjs';

const defaultOptions = {
	isTextarea: true,
	classes: [],
	color: null
};

const messageClrs = ['#362', '#874', '#833'];

class DialogMessageInput extends DialogTextInput {
	constructor(options = {}) {
		options = {
			...defaultOptions,
			...options
		};

		super(options);

		const input = this._input;

		if (null != options.color) {
			input.style.backgroundColor = options.color;
		}

		const txaWrap = document.createElement('div');
		txaWrap.className = 'txa-wrap';

		txaWrap.append(input);

		this._labelWrap.replaceWith(txaWrap);
		this._labelWrap = txaWrap;

		const delBtn = document.createElement('i');
		delBtn.className = 'msg-del-btn';
		delBtn.title = 'Удалить';
		delBtn.append(
			document.createRange().createContextualFragment(
				'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z"/></svg>'
			)
		);

		txaWrap.append(delBtn);

		delBtn.addEventListener(pointerup, () => this.emit('delete'));

		const clrsWrap = document.createElement('div');
		clrsWrap.className = 'clrs-wrap';

		for (let i = 0; i < 4; ++i) {
			const clrBtn = document.createElement('i');
			clrBtn.classList.add(['tl', 'tr', 'bl', 'br'][i]);
			const clr = messageClrs[i];
			if (null != clr) {
				clrBtn.style.backgroundColor = clr;
			}
			if (clr == options.color) { // eslint-disable-line eqeqeq
				clrBtn.classList.add('selected');
			}

			clrBtn.addEventListener(pointerup, () => {
				for (const btn of clrsWrap.childNodes) {
					if (btn === clrBtn) {
						btn.classList.add('selected');
					} else {
						btn.classList.remove('selected');
					}
				}
				this.emit('selectcolor', clr);
			});

			clrsWrap.append(clrBtn);
		}

		txaWrap.append(clrsWrap);
	}

	setColor(color) {
		this._input.style.backgroundColor = color || '';
	}
}

export default DialogMessageInput;
