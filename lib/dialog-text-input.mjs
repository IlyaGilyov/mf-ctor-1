import EventEmitter from './events.mjs';

const defaultOptions = {
	isTextarea: false,
	label: null,
	name: null,
	placeholder: null,
	classes: null,
	invalidClass: null,
	charRE: null,
	fullRE: null,
	validate: null,
	messageTO: 3000,
	hideMessageOnInput: true
};

class DialogTextInput extends EventEmitter {
	constructor(options = {}) {
		super();

		options = {
			...defaultOptions,
			...options
		};

		const {
			classes,
			label,
			isTextarea,
			name,
			placeholder,
			value
		} = options;

		const wrap = document.createElement('div');
		wrap.className = 'input-wrap';

		let classesImpl = classes;
		if (null != classesImpl) {
			if ('string' === typeof classesImpl) {
				classesImpl = [classesImpl];
			}

			if (Array.isArray(classesImpl)) {
				wrap.classList.add(...classesImpl);
			}
		}

		const labelWrap = document.createElement('label');

		if (null != label) {
			const caption = document.createElement('span');
			caption.className = 'caption';
			caption.textContent = label;

			labelWrap.append(caption);
			this._caption = caption;
		}

		let input;
		if (true === isTextarea) {
			input = document.createElement('textarea');
		} else {
			input = document.createElement('input');
			input.type = 'text';
		}
		if (null != name) {
			input.name = name;
		}
		if (null != placeholder) {
			input.placeholder = placeholder;
		}

		input.value = value || '';

		labelWrap.append(input);

		const messageWrap = document.createElement('div');
		messageWrap.textContent = '...';
		messageWrap.className = 'message';

		wrap.append(labelWrap, messageWrap);

		this._value = input.value;

		this._wrap = wrap;
		this._labelWrap = labelWrap;
		this._input = input;
		this._messageWrap = messageWrap;

		this._options = options;

		this._isValid = true;

		this._setupEvents();
	}

	_setupEvents() {
		const {
			invalidClass,
			charRE,
			fullRE,
			validate,
			hideMessageOnInput
		} = this._options;

		const input = this._input;

		input.addEventListener('focus', e => {
			this._valueOnFocus = input.value;

			this.emit('focus', e);
		});

		if (charRE instanceof RegExp) {
			input.addEventListener('beforeinput', e => {
				if (null !== e.data && !charRE.test(e.data)) {
					e.preventDefault();
					this.emit('invalidchar', e.data, e);
				}
			});
		}

		if (true === hideMessageOnInput) {
			input.addEventListener('input', () => this.hideMessage());
		}

		input.addEventListener('input', e => {
			this._value = input.value;
			this.emit('input', e);
		});

		if (
			fullRE instanceof RegExp ||
			'function' === typeof validate
		) {
			if (fullRE instanceof RegExp && 'function' === typeof validate) {
				throw new Error('Can not use both validation function and regular expression');
			}

			let validateImpl = validate;
			if ('function' !== typeof validateImpl) {
				validateImpl = value => fullRE.test(value);
			}

			this._isValid = validateImpl(this._value);

			if (!this._isValid && null != invalidClass) {
				input.classList.add(invalidClass);
			}

			input.addEventListener('input', e => {
				const isValid = validateImpl(input.value);
				if (isValid !== this._isValid) {
					this.emit('validchange', isValid, e);

					if (null != invalidClass) {
						if (isValid) {
							input.classList.remove(invalidClass);
						} else {
							input.classList.add(invalidClass);
						}
					}

					this._isValid = isValid;
				}
			});
		}

		input.addEventListener('blur', e => {
			this.emit('blur', e);

			if (input.value !== this._valueOnFocus) {
				this.emit('change', e);
			}
		});
	}

	showMessage(text, type = null, to = null) {
		this.hideMessage();

		const messageWrap = this._messageWrap;
		const msg = {};

		if (null !== type) {
			messageWrap.classList.add(type);
			msg.type = type;
		}

		if (null === to) {
			to = this._options.messageTO;
		}

		messageWrap.textContent = text;
		messageWrap.classList.add('visible');

		if (isFinite(to) && 0 < to) {
			msg.timer = setTimeout(() => this.hideMessage(), to);
		}

		this._msg = msg;
	}

	hideMessage() {
		if (undefined !== this._msg) {
			const messageWrap = this._messageWrap;
			const { type, timer } = this._msg;
			if (undefined !== type) {
				messageWrap.classList.remove(type);
			}

			clearTimeout(timer);
			messageWrap.classList.remove('visible');

			delete this._msg;
		}
	}

	getValue() {
		return this._value;
	}

	isValid() {
		return this._isValid;
	}

	getElement() {
		return this._wrap;
	}

	getInputElement() {
		return this._input;
	}
}

export default DialogTextInput;
