import EventEmitter from './events.mjs';

import {
	pointerdown,
	pointerup/*,
	pointermove,
	pointercancel*/
} from './pointer-event-names.mjs';

const defaultOptions = {
	vMargin: 24,
	buttons: null,
	classes: null,
	closeBtn: true,
	closeOnPtrOutside: true,
	ignoreContents: false,
	ignorePosition: false
};

class Dialog extends EventEmitter {
	constructor(contents, options = {}) {
		super();

		options = {
			...defaultOptions,
			...options
		};

		const screen = document.createElement('div');
		screen.className = 'dialog-screen';

		const win = document.createElement('div');
		win.className = 'dialog-window';

		const wrap = document.createElement('div');
		wrap.className = 'content-wrap';

		let { classes } = options;
		if (null != classes) {
			if ('string' === typeof classes) {
				classes = [classes];
			}

			if (Array.isArray(classes)) {
				win.classList.add(...classes);
			}
		}

		win.append(wrap);
		screen.append(win);

		this._screen = screen;
		this._win = win;
		this._wrap = wrap;

		this._options = options;

		if (true !== options.ignoreContents) {
			this.addContents(contents);
		}

		document.body.append(screen);

		if (Array.isArray(options.buttons) && 0 !== options.buttons.length) {
			this._setupButtons();
		}

		if (true === options.closeBtn) {
			this._setupCloseBtn();
		}

		if (true === options.closeOnPtrOutside) {
			this._setupPtrOutside();
		}

		if (true !== options.ignorePosition) {
			this.updatePosition();
		}

		this._onResize = () => this.updatePosition();
		window.addEventListener('resize', this._onResize);
	}

	_setupButtons() {
		const { buttons } = this._options;
		const win = this._win;

		win.classList.add('w-buttons');

		const buttonsWrap = document.createElement('div');
		buttonsWrap.className = 'buttons-wrap';

		const names = new Set();

		for (const { name, caption, className } of buttons) {
			if ('string' !== typeof name || 0 === name.length) {
				throw new Error('Button name is required');
			}

			if (names.has(name)) {
				throw new Error('Button name has to be unique');
			}

			names.add(name);

			const button = document.createElement('input');
			button.type = 'button';
			button.name = name;
			button.value = 'string' === typeof caption && 0 !== caption.length ? caption : name;

			if ('string' === typeof className && 0 !== className.length) {
				button.className = className;
			}

			button.classList.add('button', name);

			const eventName = `button:${name}`;
			button.addEventListener(pointerup, e => {
				if (isFinite(e.buttons) && 0 !== e.button) {
					return;
				}


				this.emit(eventName);
			});

			buttonsWrap.append(button);
		}

		this._buttonsWrap = buttonsWrap;

		win.append(buttonsWrap);
	}

	_setupCloseBtn() {
		const closeBtn = document.createElement('i');
		closeBtn.className = 'close-btn';
		closeBtn.title = 'Закрыть';
		closeBtn.append(
			document.createRange().
				createContextualFragment(
					'<svg xmlns="http://www.w3.org/2000/svg"><line x2="100%" y2="100%"/><line y1="100%" x2="100%"/></svg>'
				)
		);

		closeBtn.addEventListener(pointerup, e => {
			if (isFinite(e.buttons) && 0 !== e.button) {
				return;
			}

			this.close();
		});

		this._win.append(closeBtn);

		this._closeBtn = closeBtn;
	}

	_setupPtrOutside() {
		const screen = this._screen;

		screen.addEventListener(pointerdown, e => {
			if (isFinite(e.buttons) && 0 !== e.button) {
				return;
			}

			if (screen === e.target) {
				this.close();
			}
		});
	}

	addContents(contents, clear = false) {
		const wrap = this._wrap;

		if (true === clear) {
			wrap.replaceChildren();
		}

		if (contents instanceof NodeList) {
			contents = Array.from(contents);
		} else if (!Array.isArray(contents)) {
			contents = [contents];
		}

		contents = contents.filter(
			item =>
				'string' === typeof item ||
				null != item && (
					Node.ELEMENT_NODE === item.nodeType ||
					Node.TEXT_NODE === item.nodeType ||
					Node.DOCUMENT_FRAGMENT_NODE === item.nodeType
				)
		);

		if (0 !== contents.length) {
			wrap.append(...contents);
		}
	}

	updatePosition() {
		const { vMargin } = this._options;
		const win = this._win;
		const winHeight = win.offsetHeight;
		const screenHeight = this._screen.offsetHeight;

		const top = Math.max(
			(screenHeight - winHeight) / 2,
			vMargin
		);

		win.style.marginTop = `${top}px`;
	}

	close() {
		this._screen.remove();

		window.removeEventListener('resize', this._onResize);

		this.emit('close');
	}
}

export default Dialog;
