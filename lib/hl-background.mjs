import SimplexNoise from './simplex-noise.mjs';
import ClockGenerator from './clock-generator.mjs';
import EventEmitter from './events.mjs';
import TycheI from './tyche-i.mjs';
import lerp from './lerp.mjs';

const now = performance.now.bind(performance);

const defaultBlend = (
	rgbaBg, rgbaFg, result = Array(4),
	rgbaBgOffset = 0, rgbaFgOffset = 0, resultOffset = 0
) => {
	const abg = rgbaBg[rgbaBgOffset + 3];
	const afg = rgbaFg[rgbaFgOffset + 3];
	const ares = afg + abg * (255 - afg) / 255;
	const ablend = ares !== 0 ? afg * 255 / ares : 255;
	result[resultOffset + 0] = Math.round(
		(rgbaBg[rgbaBgOffset + 0] * (255 - ablend) + rgbaFg[rgbaFgOffset + 0] * ablend) / 255
	);
	result[resultOffset + 1] = Math.round(
		(rgbaBg[rgbaBgOffset + 1] * (255 - ablend) + rgbaFg[rgbaFgOffset + 1] * ablend) / 255
	);
	result[resultOffset + 2] = Math.round(
		(rgbaBg[rgbaBgOffset + 2] * (255 - ablend) + rgbaFg[rgbaFgOffset + 2] * ablend) / 255
	);
	result[resultOffset + 3] = Math.round(ares);
	return result;
};

const TAU = Math.PI * 2;

const transformUV = (u, v, flipU = false, flipV = false) => {
	const vx = u - 0.5;
	const vy = v - 0.5;
	let a = Math.atan2(vy, vx);
	if (a < 0) {
		a += TAU;
	}
	u = a / TAU;
	v = Math.sqrt(vx * vx + vy * vy) * 2;
	v = Math.pow(v, 1 / 2);
	if (true === flipU) { u = 1 - u; }
	if (true === flipV) { v = 1 - v; }
	return [u, v];
};

const getColor = () => {
	const color = [
		Math.random(),
		Math.random(),
		Math.random()
	];
	const mul = 1 / Math.max(...color);
	color[0] *= mul;
	color[1] *= mul;
	color[2] *= mul;
	return color;
};

const saturate = rgb => {
	if (rgb[0] > 1.0) { rgb[1] += rgb[0] - 1.0; }
	if (rgb[1] > 1.0) { rgb[2] += rgb[1] - 1.0; }
	if (rgb[2] > 1.0) { rgb[0] += rgb[2] - 1.0; }
	return rgb;
};

const defaultPulseOptions = {
	x: 0.5,
	y: 0.5,
	width: 1,
	height: 1,
	vx: 1 / 8, // 1 / 1e3, // velocity x
	vy: 1 / 2, // 1 / 100, // velocity x
	vz: 1 / 2, // 1 / 200, // velocity x
	nfx: 3, // noise factor x
	nfy: 5, // noise factor y
	nfz: 3, // noise factor z
	nf2x: 8, // 2d noise factor x
	nf2y: 1, // 2d noise factor y
	timeFrame: 1000,
	repeat: 1,
	t: 0,
	noise: null,
	color: null,
	blend: defaultBlend
};

const rgba0 = new Uint8ClampedArray(4);

class Pulse extends EventEmitter {
	constructor(options = {}) {
		super();

		this._options = {
			...defaultPulseOptions,
			...options
		};

		const {
			noise,
			color
		} = this._options;

		this._noise = noise instanceof SimplexNoise ? noise : new SimplexNoise();
		this._color = Array.isArray(color) ? color : getColor();

		this._t = this._options.t;
	}

	_render(imgData, t = this._t) {
		const {
			x, y,
			width: rw,
			height: rh,
			vx, vy, vz,
			nfx, nfy, nfz,
			nf2x, nf2y,
			blend
		} = this._options;

		const noise = this._noise;

		const color = this._color;

		const { width: cw, height: ch, data } = imgData;

		const aw = Math.ceil(cw * rw);
		const ah = Math.ceil(ch * rh);
		const ar = aw * ah;

		const acx = Math.floor(cw * x);
		const acy = Math.floor(ch * y);

		const ax = Math.ceil(acx - aw / 2);
		const ay = Math.ceil(acy - ah / 2);

		for (let i = 0; i < ar; ++i) {
			const rx = i % aw;
			const ry = i / aw | 0;

			const [u, v] = transformUV(
				rx / (aw - 1), // u
				ry / (ah - 1), // v
				true, true
			);

			const ru = u > 0.5 ? 1 - u : u;
			let nv0 = noise.noise(
				ru * nf2x,
				t * nf2y
			);
			nv0 = (1 + nv0) / 2;
			nv0 = (v * 2 + nv0 * v) * Math.pow(1 - t, 2);

			let nv1 = noise.noise3D(
				(u + t * vx) * nfx,
				(v + t * vy) * nfy,
				t * vz * nfz
			);
			if (u < 1 / 3) {
				nv1 = lerp(
					nv1,
					noise.noise3D(
						(u + 1 + t * vx) * nfx,
						(v + t * vy) * nfy,
						t * vz * nfz
					),
					1 - u / (1 / 3)
				);
			}
			nv1 = (1 + nv1) / 2;
			nv1 *= v * 2 * Math.pow(1 - t, 1 / 2);

			const nv = nv0 + nv1;

			const rgba = saturate([
				color[0] * nv,
				color[1] * nv,
				color[2] * nv,
				nv
			]);

			const idx = ((ay + ry) * cw + ax + rx) * 4;

			rgba0[0] = Math.round(rgba[0] * 255);
			rgba0[1] = Math.round(rgba[1] * 255);
			rgba0[2] = Math.round(rgba[2] * 255);
			rgba0[3] = Math.round(rgba[3] * 255);

			blend(
				data, rgba0, data,
				idx, 0, idx
			);
		}
	}

	frame(imgData, currentTime = now()) {
		if (true !== this._complete) {
			const {
				timeFrame,
				repeat
			} = this._options;

			let t = this._t;

			if (undefined !== this._prevTime) {
				t = Math.min(repeat, t + (currentTime - this._prevTime) / timeFrame);
				this._t = t;
			}

			this._render(imgData, t);

			this._prevTime = currentTime;

			if (repeat <= t) {
				delete this._prevTime;
				this._complete = true;
				this.emit('complete');
			}
		}
	}
}

class HlBackground {
	constructor(wrap, canvasWidth = 320, canvasHeight = 240) {
		const canvas = document.createElement('canvas');
		canvas.width = canvasWidth;
		canvas.height = canvasHeight;
		canvas.style.width = '100%';
		canvas.style.height = '100%';

		const ctx = canvas.getContext('2d');

		wrap.append(canvas);

		this._pulses = new Set();

		this._imgData = new ImageData(canvasWidth, canvasHeight);

		this._canvas = canvas;
		this._ctx = ctx;
		this._wrap = wrap;

		this._clock = new ClockGenerator({
			frequency: Infinity,
			autoRun: true,
			forceContext: this
		});
	}

	pulse(x, y, size, options = {}) {
		const clock = this._clock;
		const wrap = this._wrap;

		if (!isFinite(options.x)) { options.x = x / (wrap.offsetWidth - 1); }
		if (!isFinite(options.y)) { options.y = y / (wrap.offsetHeight - 1); }

		if (!isFinite(options.width)) { options.width = size / wrap.offsetWidth; }
		if (!isFinite(options.height)) { options.height = size / wrap.offsetHeight; }

		if (!(options.noise instanceof SimplexNoise)) {
			options.noise = new SimplexNoise(new TycheI().getRandom());
		}

		const pulses = this._pulses;
		const pulse = new Pulse(options);
		if (0 === pulses.size) {
			clock.addListener(this.frame);
		}

		pulse.addListener('complete', () => {
			pulses.delete(pulse);
			if (0 === pulses.size) {
				clock.removeListener(this.frame);
			}
		});

		pulses.add(pulse);
		return pulse;
	}

	pulseElement(element, mul, options) {
		const { top, left, width, height } = element.getBoundingClientRect();

		return this.pulse(
			left + width / 2,
			top + height / 2,
			Math.sqrt(width * width + height * height) * mul,
			options
		);
	}

	frame(currentTime) {
		const imgData = this._imgData;
		imgData.data.fill(0);

		const pulses = this._pulses;
		for (const pulse of pulses.values()) {
			pulse.frame(imgData, currentTime);
		}

		this._ctx.putImageData(imgData, 0, 0);
	}
}

HlBackground.Pulse = Pulse;

export default HlBackground;
