import clamp from './clamp.mjs';
import lerp from './lerp.mjs';

import EventEmitter from './events.mjs';

import { nanoid } from './nanoid.mjs';

import {
	pointerdown,
	pointerup,
	pointermove/*,
	pointercancel*/
} from './pointer-event-names.mjs';

import {
	onPress/*,
	offPress*/
} from './pointer-event-helpers.mjs';

const rgbToHsl = (r, g, b, values = Array(3)) => {
	r /= 255;
	g /= 255;
	b /= 255;

	const max = Math.max(r, g, b);
	const min = Math.min(r, g, b);

	let h;
	let s;
	const l = (max + min) / 2;

	if (max === min) {
		h = 0;
		s = 0;
	} else {
		const d = max - min;
		s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
		if (max === r) {
			h = (g - b) / d + (g < b ? 6 : 0);
		} else if (max === g) {
			h = (b - r) / d + 2;
		} else {
			h = (r - g) / d + 4;
		}
		h /= 6;
	}

	values[0] = h;
	values[1] = s;
	values[2] = l;

	return values;
};

const hslToRgb = (h, s, l, values = Array(3)) => {
	h -= Math.floor(h);
	h *= 6;
	const c = s * (1 - Math.abs(2 * l - 1));
	const x = c * (1 - Math.abs(h % 2 - 1));
	const m = l - c / 2;
	let r; let g; let b;
	if (h < 1) {
		r = c; g = x; b = 0;
	} else if (h < 2) {
		r = x; g = c; b = 0;
	} else if (h < 3) {
		r = 0; g = c; b = x;
	} else if (h < 4) {
		r = 0; g = x; b = c;
	} else if (h < 5) {
		r = x; g = 0; b = c;
	} else {
		r = c; g = 0; b = x;
	}

	values[0] = clamp(Math.round((r + m) * 255), 0, 255);
	values[1] = clamp(Math.round((g + m) * 255), 0, 255);
	values[2] = clamp(Math.round((b + m) * 255), 0, 255);

	return values;
};

const DRG_CLONES_COUNT = 3;
const DRG_MAG_DIST = 0.25;

const anim = ({
	el, tgt,
	time = 300,
	fn = t => t * t * t,
	onFrame = () => {}
}) => new Promise(resolve => {
	let startX;
	let startY;
	if (Array.isArray(el)) {
		startX = Array(el.length);
		startY = Array(el.length);
		for (let i = 0; i < el.length; ++i) {
			const rect = el[i].getBoundingClientRect();
			startX[i] = rect.left + rect.width / 2;
			startY[i] = rect.top + rect.height / 2;
		}
	} else {
		const rect = el.getBoundingClientRect();
		startX = rect.left + rect.width / 2;
		startY = rect.top + rect.height / 2;
	}

	const startTime = performance.now();
	let t = 0;

	const iter = () => {
		if (t < 1) {
			requestAnimationFrame(iter);
		}

		const currentTime = performance.now();
		const dt = currentTime - startTime;

		t = Math.min(1, dt / time);

		const ti = fn(t);

		if (Array.isArray(el)) {
			for (let i = 0; i < el.length; ++i) {
				if (isFinite(tgt.x)) { el[i].style.left = `${lerp.round(startX[i], tgt.x, ti)}px`; }
				if (isFinite(tgt.y)) { el[i].style.top = `${lerp.round(startY[i], tgt.y, ti)}px`; }
			}
		} else {
			if (isFinite(tgt.x)) { el.style.left = `${lerp.round(startX, tgt.x, ti)}px`; }
			if (isFinite(tgt.y)) { el.style.top = `${lerp.round(startY, tgt.y, ti)}px`; }
		}

		onFrame(ti, t);

		if (1 === t) {
			resolve();
		}
	};

	requestAnimationFrame(iter);
});

const parseCSSAngle = cssAngle => {
	let res = cssAngle;

	if ('string' === typeof res) {
		const cssalc = res.toLowerCase();
		res = parseFloat(cssalc);

		if (isFinite(res)) {
			if (cssalc.includes('deg')) {
				res *= Math.PI / 180;
			} else if (cssalc.includes('grad')) {
				res *= Math.PI / 200;
			} else if (cssalc.includes('turn')) {
				res *= Math.PI * 2;
			} else if (!cssalc.includes('rad')) {
				throw new Error(`Unknown css angle unit "${cssAngle.replace(/^[^\D.]+/, '')}"`);
			}
		}
	} else if (0 !== res) {
		res = NaN;
	}

	if (isNaN(res)) {
		throw new Error(`Invalid css angle value "${cssAngle}"`);
	}

	return res;
};

class Messages extends EventEmitter {
	constructor(cnt, closeBtn, arrowLeft, arrowRight, items) {
		super();

		if (0 === items.length) {
			throw new Error('Message items list is empty');
		}

		this._cnt = cnt;

		this._closeBtn = closeBtn;
		this._arrowLeft = arrowLeft;
		this._arrowRight = arrowRight;

		this._items = items;
		this._mult = 1 < items.length;

		this._setup();
	}

	_setup() {
		const dlg = document.createElement('div');
		dlg.classList.add('messages-dlg', 'opening', 'translated');
		setTimeout(() => {
			dlg.classList.remove('translated');
			const onceTransitionEnd = () => {
				dlg.removeEventListener('transitionend', onceTransitionEnd);
				dlg.classList.remove('opening');
			};
			dlg.addEventListener('transitionend', onceTransitionEnd);
		}, 100);

		if (!this._mult) {
			dlg.classList.add('single');
		}

		const items = this._items;
		const elements = Array(items.length);
		for (let i = 0; i < items.length; ++i) {
			const item = items[i];
			const element = document.createElement('p');
			element.textContent = 'string' === typeof item ? item : 'string' === typeof item.text ? item.text : 'Нет сообщения';
			elements[i] = element;
		}

		const wrap = document.createElement('div');
		wrap.className = 'wrap';

		const arrowLeft = this._arrowLeft;
		const arrowRight = this._arrowRight;

		arrowLeft.classList.add('inactive');
		arrowRight.classList.add('inactive');

		dlg.append(wrap, arrowLeft, arrowRight, this._closeBtn);

		this._elements = elements;
		this._dlg = dlg;
		this._wrap = wrap;

		this._zIdx = 1e3;

		arrowLeft.addEventListener(pointerdown, e => e.preventDefault());
		arrowRight.addEventListener(pointerdown, e => e.preventDefault());

		arrowLeft.addEventListener(pointerup, e => { this.prev(); e.preventDefault(); });
		arrowRight.addEventListener(pointerup, e => { this.next(); e.preventDefault(); });

		this._closeBtn.addEventListener(pointerup, () => this.close());

		this.next();

		this._cnt.append(dlg);
	}

	_updateColor(itemIdx) {
		let clrValue = '';

		const item = this._items[itemIdx];
		if (null != item && null != item.color) {
			clrValue = item.color;
		}

		this._dlg.style.backgroundColor = clrValue;
	}

	_setupNextTimer(curItemIdx) {
		const items = this._items;
		const item = items[curItemIdx];
		const isLast = curItemIdx === items.length - 1;
		this._nextTimer = setTimeout(
			() => isLast ? this.close() : this.next(),
			null != item && 0 < item.time ? item.time : 2000
		);
	}

	_switchMsg(dir) {
		clearTimeout(this._nextTimer);

		const wrap = this._wrap;
		const elements = this._elements;
		const arrowLeft = this._arrowLeft;
		const arrowRight = this._arrowRight;

		if (!isFinite(this._idx)) {
			wrap.append(elements[0]);
			if (this._mult) {
				arrowRight.classList.remove('inactive');
			}

			this._updateColor(0);

			this._setupNextTimer(0);

			this._idx = 0;

			const queue = Promise.resolve();
			this._msgQueue = queue;
			return queue;
		}

		const prevIdx = this._idx;
		const nextIdx = (prevIdx + dir) % elements.length;
		if (nextIdx === prevIdx || !isFinite(nextIdx)) {
			return this._msgQueue;
		}

		if (0 === nextIdx) {
			arrowLeft.classList.add('inactive');
		} else {
			arrowLeft.classList.remove('inactive');
		}

		if (elements.length - 1 === nextIdx) {
			arrowRight.classList.add('inactive');
		} else {
			arrowRight.classList.remove('inactive');
		}

		const prevElement = elements[prevIdx];
		const nextElement = elements[nextIdx];

		const queue = this._msgQueue.then(() => new Promise((resolve/*, reject*/) => {
			if (true === this._destroyed) {
				resolve();
				return;
			}

			const wrapHeight = wrap.offsetHeight;
			wrap.style.height = `${wrapHeight}px`;

			const prevElementHeight = prevElement.offsetHeight;
			prevElement.classList.add('t8n');
			const nextElementClassName = dir > 0 ? 'translate-right' : 'translate-left';
			nextElement.classList.add('t8n', nextElementClassName);

			wrap.append(nextElement);

			requestAnimationFrame(() => {
				wrap.style.height = `${wrapHeight + (nextElement.offsetHeight - prevElementHeight)}px`;
				const prevElementClassName = dir > 0 ? 'translate-left' : 'translate-right';
				prevElement.classList.add(prevElementClassName);
				nextElement.classList.remove(nextElementClassName);

				const onceTransitionEnd = () => {
					wrap.removeEventListener('transitionend', onceTransitionEnd);
					prevElement.classList.remove(prevElementClassName, 't8n');
					nextElement.classList.remove('t8n');

					prevElement.remove();

					this._setupNextTimer(nextIdx);

					resolve();
				};
				wrap.addEventListener('transitionend', onceTransitionEnd);
			}); // TODO: Maybe repl w To

			this._updateColor(nextIdx);
		}));

		this._idx = nextIdx;

		this._msgQueue = queue;
		return queue;
	}

	next() {
		return this._switchMsg(1);
	}

	prev() {
		return this._switchMsg(-1);
	}

	close() {
		const dlg = this._dlg;
		dlg.classList.add('closing', 'translated');
		const onceTransitionEnd = () => {
			dlg.removeEventListener('transitionend', onceTransitionEnd);
			this.destroy();
		};
		dlg.addEventListener('transitionend', onceTransitionEnd);
		this.emit('close');
	}

	destroy() {
		this._destroyed = true;
		this._dlg.remove();
		this.emit('destroy');
	}
}

class Layout extends EventEmitter {
	constructor({
		toolsBtn,
		toolsCloseBtn,
		toolsField,
		toolsWrap,
		toolsCnt,
		toolHint,
		textField,
		photoField,
		messagesCnt = null,

		heightMargin = 80,
		widthFactor = 0.6,
		sideWidth = 360,
		sideMargin = 40,

		resize = false
	}) {
		super();

		this._toolsBtn = toolsBtn;
		this._toolsCloseBtn = toolsCloseBtn;
		this._toolsField = toolsField;
		this._toolsWrap = toolsWrap;
		this._toolsCnt = toolsCnt;
		this._toolHint = toolHint;
		this._textField = textField;
		this._photoField = photoField;
		if (null !== messagesCnt) {
			this._messagesCnt = messagesCnt;
		} else {
			this._messagesCnt = this._photoField;
		}

		this._heightMargin = heightMargin;
		this._widthFactor = widthFactor;
		this._sideWidth = sideWidth;
		this._sideMargin = sideMargin;

		this._placedItems = [];

		this._setupToolsOpen();

		if (true === resize) {
			this.resize();
		}
	}

	fillText(headingHTML, paragraphHTML) {
		const heading = document.createElement('h1');
		heading.innerHTML = headingHTML;

		const paragraphWrap = document.createElement('div');
		paragraphWrap.className = 'p-wrap';
		paragraphWrap.innerHTML = paragraphHTML;

		const section = this._textField;
		section.append(heading, paragraphWrap);
	}

	toggleTools(state) {
		const currentState = true === this._toolsState;

		if ('boolean' !== typeof state) {
			state = !currentState;
		}

		if (state !== currentState) {
			this._toolsState = state;

			const toolsBtn = this._toolsBtn;
			const toolsField = this._toolsField;
			const toolsWrap = this._toolsWrap;

			if (state) {
				toolsBtn.classList.add('hidden');
				toolsBtn.classList.remove('placed');

				toolsField.classList.add('shown');
				requestAnimationFrame(() => toolsWrap.classList.add('placed')); // TODO: Maybe repl w To
			} else {
				toolsBtn.classList.remove('hidden');
				requestAnimationFrame(() => toolsBtn.classList.add('placed')); // TODO: Maybe repl w To

				toolsField.classList.remove('shown');
				toolsWrap.classList.remove('placed');
			}

			this.emit('toolsstatechange', state);
		}
	}

	_setupToolsOpen() {
		const onToolsBtnPress = () => this.toggleTools(true);
		onPress(this._toolsBtn, onToolsBtnPress);

		const onToolsCloseBtnPress = () => this.toggleTools(false);
		onPress(this._toolsCloseBtn, onToolsCloseBtnPress);

		const onDocPtrDnCloseTools = e => {
			if (!this._toolsWrap.contains(e.target)) {
				this.toggleTools(false);
			}
		};
		document.documentElement.addEventListener(pointerdown, onDocPtrDnCloseTools);

		this._onToolsBtnPress = onToolsBtnPress;
		this._onToolsCloseBtnPress = onToolsCloseBtnPress;
		this._onDocPtrDnCloseTools = onDocPtrDnCloseTools;
	}

	resize() {
		const cHeight = window.innerHeight - this._heightMargin;
		const cWidth = cHeight * this._widthFactor;

		const sWidth = (window.innerWidth - cWidth) / 2;
		const sMgn = sWidth - this._sideWidth;

		const reduced = sMgn < this._sideMargin;

		const photoField = this._photoField;

		this._scale = photoField.offsetHeight / 500;

		for (const placedItem of this._placedItems) {
			const { allocItem, cnt } = placedItem;

			const rect = photoField.getBoundingClientRect();
			const x = rect.left + rect.width * allocItem.posX;
			const y = rect.top + rect.height * allocItem.posY;

			cnt.style.left = `${x}px`;
			cnt.style.top = `${y}px`;

			let scaleX = 1;
			let scaleY = 1;

			if (isFinite(allocItem.scale)) {
				scaleX = allocItem.scale;
				scaleY = allocItem.scale;
			} else {
				if (isFinite(allocItem.scaleX)) { scaleX = allocItem.scaleX; }
				if (isFinite(allocItem.scaleY)) { scaleY = allocItem.scaleY; }
			}

			scaleX *= this._scale;
			scaleY *= this._scale;

			let transform;
			if (1 !== scaleX || 1 !== scaleY) {
				transform = `scale(${scaleX}, ${scaleY})`;
			}

			if (null != allocItem.rotate) {
				if (null != transform) {
					transform += ' ';
				} else {
					transform = '';
				}

				transform += `rotate(${allocItem.rotate})`;
			}

			cnt.style.transform = transform;
		}

		if (reduced !== this._reduced) {
			this._reduced = reduced;
			this.emit('change', reduced);
		}
	}

	isReduced() {
		return true === this._reduced;
	}

	_setupToolBtn(btnEl, toolEntry) {
		const svg = toolEntry.iconSVG || toolEntry.svg;

		btnEl.addEventListener('mouseover', () => this.setHint(toolEntry.captionOverride || toolEntry.caption));
		btnEl.addEventListener('mouseout', () => this.setHint());

		btnEl.append(svg);
	}

	setHint(value = null) {
		clearTimeout(this._clearHintTimer);

		if (null == value || 'string' === typeof value && '' === value.trim()) {
			this._clearHintTimer = setTimeout(() => { this._toolHint.textContent = ''; }, 500);
		} else {
			this._toolHint.textContent = value;
		}
	}

	setupTools(tools) {
		if (null != this._tools) {
			return;
		}

		if (true === this._reduced && true !== this._toolsState) {
			this.once('toolsstatechange', () => this.setupTools(tools));
			this.once('change', () => this.setupTools(tools));
			return;
		}

		const toolsCnt = this._toolsCnt;
		const toolBtns = [];

		for (let i = 0; i < tools.length; ++i) {
			const toolEntry = tools[i];

			const el = document.createElement('i');
			el.className = 'tool-btn';
			el.dataset.name = toolEntry.name;
			toolsCnt.append(el);

			toolBtns.push(el);

			this._setupToolBtn(el, toolEntry);
		}

		this._tools = tools;
		this._toolBtns = toolBtns;

		this.emit('toolsready');
	}

	_calcTintRGB() {
		const srcImg = this._origImg;
		const { width, height } = srcImg;
		const cvs = document.createElement('canvas');
		cvs.width = width;
		cvs.height = height;

		const ctx = cvs.getContext('2d');
		ctx.drawImage(srcImg, 0, 0);

		const imgData = ctx.getImageData(0, 0, width, height);
		const { data } = imgData;

		const res = width * height;
		let rsum = 0;
		let gsum = 0;
		let bsum = 0;

		for (let i = 0; i < res; ++i) {
			const idx = i * 4;

			const r = data[idx + 0];
			const g = data[idx + 1];
			const b = data[idx + 2];

			rsum += r;
			gsum += g;
			bsum += b;
		}

		const ravg = Math.round(rsum / res);
		const gavg = Math.round(gsum / res);
		const bavg = Math.round(bsum / res);

		const [hue] = rgbToHsl(ravg, gavg, bavg);

		return hslToRgb(hue, 0.75, 0.75);
	}

	_setupPhotoImpl() {
		this._tintRGB = this._calcTintRGB();

		const photoField = this._photoField;

		const origImg = this._origImg;
		origImg.classList.add('photo');
		origImg.classList.add('anim');
		origImg.classList.add('transp');
		origImg.classList.add('ptr');

		requestAnimationFrame(() => { // TODO: Maybe repl w To
			origImg.classList.remove('transp');
			const onceTransitionEnd = () => {
				origImg.classList.remove('anim');
				origImg.removeEventListener('transitionend', onceTransitionEnd);
			};
			origImg.addEventListener('transitionend', onceTransitionEnd);
		});

		photoField.append(origImg);
		photoField.classList.remove('preload');

		origImg.addEventListener(pointerup, e => {
			if (!isFinite(e.button) || 0 === e.button) {
				this.magnifyPhoto();
			}
		});

		origImg.addEventListener('usrreload', () => {
			const imageURL = new URL(origImg.src);
			imageURL.search = `?${nanoid(8)}`;

			origImg.remove();

			this.setupPhoto(imageURL);
		});

		const { width, height } = origImg;

		if (width > height) {
			origImg.classList.add('horizontal');
		}

		this.emit('photoready');
	}

	setupPhoto(imageURL) {
		const origImg = new Image();
		this._origImg = origImg;
		this._imageURL = imageURL;

		origImg.onerror = () => { throw new Error('Error loading image.'); };
		origImg.onload = () => this._setupPhotoImpl();
		origImg.src = imageURL;
	}

	pulsePhoto() {
		this.emit('pulse');
	}

	setupDrag(alloc) {
		const photoField = this._photoField;

		const doc = document.documentElement;
		const body = document.body;

		this._alloc = alloc;

		const toolsCnt = this._toolsCnt;
		const btns = this._toolBtns;

		let startPos = null;
		let foundBtn = null;
		let dragging = null;

		const cancel = () => {
			if (null !== startPos) {
				doc.removeEventListener(pointermove, onPointerMove);
				doc.removeEventListener(pointerup, cancel);
			}

			if (null != dragging) {
				const clr = dragging => {
					if (Array.isArray(dragging.clones)) {
						dragging.clones.forEach(el => { if (null != el.parentNode) { el.remove(); } });
						dragging.clones = null;
					}

					if (null != dragging.cnt && null != dragging.cnt.parentNode) {
						dragging.cnt.remove();
						dragging.cnt = null;
					}
				};

				if (true === dragging.placed) {
					const { allocItem: { posX, posY } } = dragging;
					const rect = photoField.getBoundingClientRect();
					const tgt = {
						x: rect.left + rect.width * posX,
						y: rect.top + rect.height * posY
					};

					let el;
					if (null != dragging.cnt && null != dragging.cnt.parentNode) {
						el = dragging.cnt;
					}

					if (Array.isArray(dragging.clones)) {
						el = null != el ? [el] : [];
						el.push(...dragging.clones);
					}

					const drg = dragging;
					const cnt = drg.cnt;
					delete drg.cnt;

					anim({
						el, tgt
					}).then(() => {
						clr(drg);
						drg.cnt = cnt;

						alloc.splice(alloc.indexOf(drg.allocItem), 1);
						this._placedItems.push(drg);

						const complete = 0 === alloc.length;

						if (complete) {
							this.completePhoto();
						} else {
							this.pulsePhoto();
						}

						if (null != this._hl) {
							this._hl.pulse(tgt.x, tgt.y, this._scale * 150, {
								color: [
									this._tintRGB[0] / 255,
									this._tintRGB[1] / 255,
									this._tintRGB[2] / 255
								]
							});
						}

						if (complete) {
							this._hl.pulse(
								rect.left + rect.width / 2,
								rect.top + rect.height / 2,
								this._scale * 500,
								{
									color: [
										this._tintRGB[0] / 255,
										this._tintRGB[1] / 255,
										this._tintRGB[2] / 255
									]
								}
							);
						}
					});
				} else {
					let rect;
					if (true === this._reduced && true !== this._toolsState) {
						rect = this._toolsBtn.getBoundingClientRect();
					} else if (null != foundBtn) {
						rect = foundBtn.getBoundingClientRect();
					}

					let el;
					if (null != dragging.cnt && null != dragging.cnt.parentNode) {
						el = dragging.cnt;
					}

					if (Array.isArray(dragging.clones)) {
						el = null != el ? [el] : [];
						el.push(...dragging.clones);
					}

					const tgt = undefined !== rect ? {
						x: rect.left + rect.width / 2,
						y: rect.top + rect.height / 2
					} : null;

					if (null != tgt) {
						const drg = dragging;
						anim({
							el, tgt
						}).then(() => clr(drg));
					} else {
						clr(dragging);
					}
				}
			}

			toolsCnt.classList.remove('disabled');

			startPos = null;
			foundBtn = null;
			dragging = null;
		};
		const onPointerMove = e => {
			if (isFinite(e.buttons) && 1 !== e.buttons) {
				cancel();
				return;
			}

			const posSrc = null != e.touches ? e.touches[0] : e;
			const pos = {
				x: posSrc.clientX,
				y: posSrc.clientY
			};

			const dx = pos.x - startPos.x;
			const dy = pos.y - startPos.y;
			const d = Math.sqrt(dx * dx + dy * dy);

			if (null !== dragging || d > 10) {
				if (null === dragging) {
					const name = foundBtn.dataset.name;
					const tools = this._tools;
					let toolEntry;
					for (let i = 0; i < tools.length; ++i) {
						if (name === tools[i].name) {
							toolEntry = tools[i];
							break;
						}
					}

					let allocItem;
					for (let i = 0; i < alloc.length; ++i) {
						if (name === alloc[i].name) {
							allocItem = alloc[i];
							break;
						}
					}

					let srcSvg;
					if (null != allocItem && null != allocItem.svg) {
						srcSvg = allocItem.svg;
					} else {
						srcSvg = toolEntry.svg || toolEntry.iconSVG;
					}

					if (null == srcSvg) {
						cancel();
						return;
					}

					this.toggleTools(false);

					let scaleX = 1;
					let scaleY = 1;

					if (null != allocItem) {
						if (isFinite(allocItem.scale)) {
							scaleX = allocItem.scale;
							scaleY = allocItem.scale;
						} else {
							if (isFinite(allocItem.scaleX)) { scaleX = allocItem.scaleX; }
							if (isFinite(allocItem.scaleY)) { scaleY = allocItem.scaleY; }
						}
					}

					scaleX *= this._scale;
					scaleY *= this._scale;

					let transform;
					if (1 !== scaleX || 1 !== scaleY) {
						transform = `scale(${scaleX}, ${scaleY})`;
					}

					if (null != allocItem && null != allocItem.rotate) {
						let cssRotate = allocItem.rotate;
						if (Math.sign(scaleX) !== Math.sign(scaleY)) {
							const rotation = -parseCSSAngle(cssRotate);
							cssRotate = `${rotation * 180 / Math.PI}deg`;
						}

						if (null != transform) {
							transform += ' ';
						} else {
							transform = '';
						}

						transform += `rotate(${cssRotate})`;
					}

					const cnt = document.createElement('div');
					cnt.className = 'drag-svg-cnt';
					if (null != transform) {
						cnt.style.transform = transform;
					}

					const idRpl = new Map();
					const src = srcSvg.outerHTML.
						replace(/url\s*\(\s*#([^\)\s]+)\s*\)/gi, (_, id) => {
							const newId = nanoid();
							idRpl.set(id, newId);
							return `url(#${newId})`;
						});
					const svgFrag = document.createRange().createContextualFragment(src);
					const svg = svgFrag.querySelector('svg');
					for (const el of svg.querySelectorAll('*[id]')) {
						const rplId = idRpl.get(el.id);
						if (undefined !== rplId) {
							el.id = rplId;
						}
					}
					svg.removeAttribute('id');

					cnt.append(svg);
					body.append(cnt);

					dragging = { name, srcSvg, cnt, svg, allocItem };
				}

				const { cnt, allocItem } = dragging;

				cnt.style.top = `${pos.y}px`;
				cnt.style.left = `${pos.x}px`;

				if (null != allocItem) {
					dragging.placed = false;

					const fldRect = photoField.getBoundingClientRect();

					const r = {
						x: (pos.x - fldRect.left) / fldRect.width,
						y: (pos.y - fldRect.top) / fldRect.height
					};

					const tgt = { x: allocItem.posX, y: allocItem.posY };

					const dx = tgt.x - r.x;
					const dy = tgt.y - r.y;
					const d = Math.sqrt(dx * dx + dy * dy);

					if (d < DRG_MAG_DIST) {
						if (!Array.isArray(dragging.clones)) {
							const clones = Array(DRG_CLONES_COUNT);
							for (let i = 0; i < DRG_CLONES_COUNT; ++i) {
								const clone = cnt.cloneNode(true);
								clone.style.opacity = 1 - (i + 1) / (DRG_CLONES_COUNT + 1);
								body.append(clone);
								clones[i] = clone;
							}

							dragging.clones = clones;
						}

						const maxd = Math.min(d, DRG_MAG_DIST - d);
						const s = maxd / d;

						dragging.clones.forEach((el, i) => {
							const im = (i + 1) / (DRG_CLONES_COUNT + 1);
							const rx = r.x + dx * s * im;
							const ry = r.y + dy * s * im;
							const x = fldRect.left + rx * fldRect.width;
							const y = fldRect.top + ry * fldRect.height;

							el.style.top = `${y}px`;
							el.style.left = `${x}px`;
						});

						dragging.placed = true;
					} else {
						if (Array.isArray(dragging.clones)) {
							dragging.clones.forEach(el => { if (null != el.parentNode) { el.remove(); } });
							dragging.clones = null;
						}
					}
				}
			}

			e.preventDefault();
		};
		const onPointerDown = e => {
			for (let i = 0; i < btns.length; ++i) {
				const btn = btns[i];
				if (btn.contains(e.target)) {
					foundBtn = btn;
					break;
				}
			}

			if (null !== foundBtn) {
				const posSrc = null != e.touches ? e.touches[0] : e;
				startPos = {
					x: posSrc.clientX,
					y: posSrc.clientY
				};

				doc.addEventListener(
					pointermove,
					onPointerMove,
					null != e.touches ? { passive: false } : null
				);
				doc.addEventListener(pointerup, cancel);

				toolsCnt.classList.add('disabled');

				e.preventDefault();
			}
		};

		toolsCnt.addEventListener(pointerdown, onPointerDown);
	}

	setHL(hl) {
		this._hl = hl;
	}

	magnifyPhoto() {
		const origImg = this._origImg;
		origImg.classList.add('magnified');

		const cnt = document.createElement('div');
		cnt.className = 'big-photo';

		const closeBtn = document.querySelector('.js-tools-close-btn').cloneNode(true);
		cnt.append(closeBtn);

		const { width, height } = origImg;
		const bigImg = new Image(width, height);
		bigImg.className = 'preload';
		bigImg.src = origImg.src;

		const imgAspect = width / height;
		const resize = () => {
			const { innerWidth, innerHeight } = window;
			const cntAspect = innerWidth / innerHeight;

			if (imgAspect < cntAspect) {
				bigImg.style.height = '100%';
				bigImg.style.width = 'auto';
				bigImg.style.left = `${(innerWidth - innerHeight * imgAspect) / 2}px`;
				bigImg.style.top = '';
			} else {
				bigImg.style.width = '100%';
				bigImg.style.height = 'auto';
				bigImg.style.top = `${(innerHeight - innerWidth / imgAspect) / 2}px`;
				bigImg.style.left = '';
			}
		};

		window.addEventListener('resize', resize);
		resize();

		requestAnimationFrame(() => bigImg.classList.remove('preload')); // TODO: Maybe repl w To

		cnt.addEventListener(pointerup, e => {
			if (bigImg !== e.target) {
				window.removeEventListener('resize', resize);
				cnt.remove();
				origImg.classList.remove('magnified');
			}
		});

		cnt.prepend(bigImg);

		document.body.append(cnt);
	}

	completePhoto() {
		this.emit('complete');
	}

	showMessages(...messages) {
		let msgs = this._messages;
		if (undefined !== msgs) {
			msgs.destroy();
		}

		const arrowLeft = document.createElement('i');
		arrowLeft.className = 'arrow-left-wrap';
		arrowLeft.title = 'Предыдущее сообщение';
		arrowLeft.append(document.querySelector('.js-arrow-left').cloneNode(true));

		const arrowRight = document.createElement('i');
		arrowRight.className = 'arrow-right-wrap';
		arrowRight.title = 'Следующее сообщение';
		arrowRight.append(document.querySelector('.js-arrow-right').cloneNode(true));

		msgs = new Messages(
			this._messagesCnt,
			document.querySelector('.js-tools-close-btn').cloneNode(true),
			arrowLeft, arrowRight,
			messages
		);
		msgs.on('destroy', () => {
			delete this._messages;
		});

		this._messages = msgs;
	}
}

Layout.Messages = Messages;

export default Layout;
