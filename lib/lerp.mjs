const lerp = (a, b, t) => a * (1 - t) + b * t;

/* Inverse */
lerp.t = (a, b, v) => (v - a) / (b - a);

lerp.round = (a, b, t) => Math.round(lerp(a, b, t));

export default lerp;
