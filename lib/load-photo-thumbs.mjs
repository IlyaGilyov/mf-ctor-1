const setupPhotoEl = (el, imageURL, remClass) => {
	const img = new Image();
	img.error = () => console.error(new ReferenceError(`Failed to load image "${imageURL}"`));
	img.onload = () => {
		el.style.backgroundImage = `url(${imageURL})`;
		if (null != remClass) {
			el.classList.remove(remClass);
		}
	};

	img.src = imageURL;
};

export default (selector, remClass = null, datasetKey = 'imageurl') => {
	for (const photoEl of document.querySelectorAll(selector)) {
		const imageURL = photoEl.dataset[datasetKey];
		if (null != imageURL) {
			setupPhotoEl(photoEl, imageURL, remClass);
		}
	}
};
