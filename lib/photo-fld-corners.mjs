export const addCorners = (sectionSelector = '.photo-field') => {
	const section = document.querySelector(sectionSelector);

	for (let i = 0; i < 4; ++i) {
		const corner = document.createElement('i');
		corner.classList.add('corner');
		corner.classList.add(2 > i ? 'top' : 'bottom');
		corner.classList.add(0 === i || 3 === i ? 'left' : 'right');
		section.prepend(corner);
	}
};
