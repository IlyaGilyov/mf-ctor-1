import {
	pointerdown,
	pointerup,
	pointermove,
	pointercancel
} from './pointer-event-names.mjs';

let onPress;
let offPress;

{
	const evMap = new Map();

	onPress = (el, listener) => {
		let elEvMap = evMap.get(el);
		if (undefined === elEvMap) {
			elEvMap = new Map();
			evMap.set(el, elEvMap);
		}

		if (!elEvMap.has(listener)) {
			let pointerDownEv = null;
			const onPointerUp = e => {
				if (null !== pointerDownEv) {
					listener(e, pointerDownEv);
				}
				cancel();
			};
			const cancel = () => {
				el.removeEventListener(pointermove, cancel);
				el.removeEventListener(pointercancel, cancel);
				el.removeEventListener(pointerup, onPointerUp);
				pointerDownEv = null;
			};
			const onPointerDown = e => {
				pointerDownEv = e;
				el.addEventListener(pointermove, cancel);
				el.addEventListener(pointercancel, cancel);
				el.addEventListener(pointerup, onPointerUp);
			};
			el.addEventListener(pointerdown, onPointerDown);

			elEvMap.set(listener, [onPointerDown, onPointerUp, cancel]);
		}
	};

	offPress = (el, listener) => {
		const elEvMap = evMap.get(el);
		if (undefined === elEvMap) {
			return;
		}

		if (elEvMap.has(listener)) {
			const [onPointerDown, onPointerUp, cancel] = elEvMap.get(listener);
			el.removeEventListener(pointerdown, onPointerDown);
			el.removeEventListener(pointermove, cancel);
			el.removeEventListener(pointercancel, cancel);
			el.removeEventListener(pointerup, onPointerUp);

			elEvMap.delete(listener);
			if (0 === elEvMap.size) {
				evMap.delete(el);
			}
		}
	};
}

export { onPress, offPress };
