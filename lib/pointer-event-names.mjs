let pointerdown;
let pointerup;
let pointermove;
let pointercancel;

if (window.navigator.pointerEnabled) {
	pointerdown = 'pointerdown';
	pointerup = 'pointerup';
	pointermove = 'pointermove';
	pointercancel = 'pointercancel';
} else if (window.navigator.msPointerEnabled) {
	pointerdown = 'MSPointerDown';
	pointerup = 'MSPointerUp';
	pointermove = 'MSPointerMove';
	pointercancel = 'MSPointerCancel';
} else if ('ontouchstart' in window) {
	pointerdown = 'touchstart';
	pointerup = 'touchend';
	pointermove = 'touchmove';
	pointercancel = 'touchcancel';
} else {
	pointerdown = 'mousedown';
	pointerup = 'mouseup';
	pointermove = 'mousemove';
	pointercancel = 'mouseout';
}

export {
	pointerdown,
	pointerup,
	pointermove,
	pointercancel
};
