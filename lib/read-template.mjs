import { IS_PRODUCTION } from '../config.mjs';

import { readFile } from 'fs/promises';

let readTemplate;

if (IS_PRODUCTION) {
	const cache = new Map();
	readTemplate = async path => {
		let template = cache.get(path);
		if (undefined === template) {
			template = String(await readFile(path));
			cache.set(path, template);
		}
		return template;
	};
} else {
	readTemplate = async path => String(await readFile(path));
}

export default readTemplate;
