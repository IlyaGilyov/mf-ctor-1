import Dialog from './dialog.mjs';
import DialogTextInput from './dialog-text-input.mjs';
import DialogMessageInput from './dialog-message-input.mjs';
import { nanoid } from './nanoid.mjs';

import {
	// pointerdown,
	pointerup/*,
	pointermove,
	pointercancel*/
} from './pointer-event-names.mjs';

const defaultOptions = {
	buttons: [
		{ name: 'cancel', caption: 'Отмена' },
		{ name: 'save', caption: 'Сохранить', className: 'ok' }
	],
	classes: ['form'],
	ignoreContents: true,
	ignorePosition: true,
	closeOnPtrOutside: false
};

class SettingsDialog extends Dialog {
	constructor(path, data, options = {}) {
		super(null, {
			...defaultOptions,
			...options
		});

		const dataCpy = JSON.parse(JSON.stringify(data));
		this._dataCpy = dataCpy;

		const fragment = document.createDocumentFragment();

		const settingsSection = document.createElement('div');
		settingsSection.className = 'section settings';
		fragment.append(settingsSection);
		this._settingsSection = settingsSection;

		const pathInput = new DialogTextInput({
			name: 'path',
			label: 'Адрес страницы:',
			placeholder: path,
			value: path,
			invalidClass: 'error',
			charRE: /[\w\/]/,
			validate: value =>
				/^\/[\w\/]+$/.test(value) && value.lastIndexOf('/') < value.length - 1
		});

		settingsSection.append(pathInput.getElement());

		const titleInput = new DialogTextInput({
			name: 'title',
			label: 'Заголовок:',
			placeholder: dataCpy.title || 'Новый',
			value: dataCpy.title,
			invalidClass: 'error',
			fullRE: /^.+$/
		});

		settingsSection.append(titleInput.getElement());

		pathInput.on('input', () => setTimeout(() => this._validate(), 50));
		titleInput.on('input', () => setTimeout(() => {
			if (titleInput.isValid()) {
				dataCpy.title = titleInput.getValue().trim();
			}

			this._validate();
		}, 50));

		this._msgsInputs = new Map([
			['start', new Map()],
			['complete', new Map()]
		]);

		const startMsgsSection = document.createElement('div');
		startMsgsSection.className = 'section messages start';
		fragment.append(startMsgsSection);
		this._startMsgsSection = startMsgsSection;

		const startMsgsSectionTitle = document.createElement('h3');
		startMsgsSectionTitle.textContent = 'Сообщения на старте';
		startMsgsSection.append(startMsgsSectionTitle);

		if (Array.isArray(dataCpy.start_messages)) {
			for (const message of dataCpy.start_messages) {
				this.addMessage('start', message, false);
			}
		}

		this._createPlusBtn(startMsgsSection, button => {
			const msg = { text: '' };
			if (!Array.isArray(dataCpy.start_messages)) {
				dataCpy.start_messages = [];
			}
			dataCpy.start_messages.push(msg);
			this.addMessage('start', msg, true, button);

			this.updatePosition();
		});

		const completeMsgsSection = document.createElement('div');
		completeMsgsSection.className = 'section messages complete';
		fragment.append(completeMsgsSection);
		this._completeMsgsSection = completeMsgsSection;

		const completeMsgsSectionTitle = document.createElement('h3');
		completeMsgsSectionTitle.textContent = 'Сообщения в конце';
		completeMsgsSection.append(completeMsgsSectionTitle);

		if (Array.isArray(dataCpy.complete_messages)) {
			for (const message of dataCpy.complete_messages) {
				this.addMessage('complete', message, false);
			}
		}

		this._createPlusBtn(completeMsgsSection, button => {
			const msg = { text: '' };
			if (!Array.isArray(dataCpy.complete_messages)) {
				dataCpy.complete_messages = [];
			}
			dataCpy.complete_messages.push(msg);
			this.addMessage('complete', msg, true, button);

			this.updatePosition();
		});

		this._inputs = new Map([
			['path', pathInput],
			['title', titleInput]
		]);

		this._wrap.append(fragment);

		this.updatePosition();
	}

	_validate() {
		const pathInput = this._inputs.get('path');
		const titleInput = this._inputs.get('title');

		const valid = pathInput.isValid() && titleInput.isValid();

		this.toggleDisableButton('save', !valid);
	}

	_createPlusBtn(startMsgsSection, listener) {
		const button = document.createElement('input');
		button.type = 'button';
		button.className = 'plus-btn';
		button.value = '＋ Добавить';

		button.addEventListener(pointerup, () => listener(button));

		startMsgsSection.append(button);
	}

	toggleDisableButton(name, state) {
		const button = this._buttonsWrap.querySelector(`.button.${name}`);
		const currentState = button.classList.contains('disabled');

		if ('boolean' !== typeof state) {
			state = !currentState;
		}

		if (state !== currentState) {
			if (state) {
				button.classList.add('disabled');
			} else {
				button.classList.remove('disabled');
			}
		}
	}

	addMessage(type, message, focus = true, nextSibling = null) {
		let msgSection;
		let inputList;
		if ('start' === type) {
			msgSection = this._startMsgsSection;
			inputList = this._msgsInputs.get('start');
		} else if ('complete' === type) {
			msgSection = this._completeMsgsSection;
			inputList = this._msgsInputs.get('complete');
		}

		const input = new DialogMessageInput({
			name: `msg-${nanoid(8)}`,
			value: message.text,
			color: message.color
		});

		input.on('input', () => {
			message.text = input.getValue().trim();

			this._validate();
		});

		input.on('delete', () => {
			this.removeMessage(message);

			this._validate();
		});

		input.on('selectcolor', clr => {
			if (null == clr) {
				delete message.color;
			} else {
				message.color = clr;
			}

			input.setColor(clr);

			this._validate();
		});

		inputList.set(message, input);

		if (null !== nextSibling) {
			nextSibling.parentNode.insertBefore(input.getElement(), nextSibling);
		} else {
			msgSection.append(input.getElement());
		}

		if (true === focus) {
			input.getInputElement().focus();
		}
	}

	removeMessage(message) {
		let inputList;
		let msgList;
		if (this._msgsInputs.get('start').has(message)) {
			inputList = this._msgsInputs.get('start');
			msgList = this._dataCpy.start_messages;
		} else if (this._msgsInputs.get('complete').has(message)) {
			inputList = this._msgsInputs.get('complete');
			msgList = this._dataCpy.complete_messages;
		} else {
			throw new ReferenceError('Message not found');
		}

		const input = inputList.get(message);
		input.getElement().remove();

		msgList.splice(msgList.indexOf(message), 1);

		this.updatePosition();
	}

	getInput(name) {
		return this._inputs.get(name);
	}

	getPath() {
		return this._inputs.get('path').getValue();
	}

	getData() {
		return this._dataCpy;
	}
}

export default SettingsDialog;
