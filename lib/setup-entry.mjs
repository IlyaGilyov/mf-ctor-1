const setupEntry = () => {
	const { entryData } = window;

	const srcImage = document.querySelector('.js-photo');
	const imageURL = srcImage.src;
	srcImage.remove();
	entryData.imageURL = imageURL;

	const { tools } = entryData;
	for (const toolEntry of tools) {
		toolEntry.svg = document.querySelector(toolEntry.svg_selector);
		if (null != toolEntry.icon_svg_selector) {
			toolEntry.iconSVG = document.querySelector(toolEntry.icon_svg_selector);
		}
	}

	return entryData;
};

export default setupEntry;
