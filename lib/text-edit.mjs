import EventEmitter from './events.mjs';

import debounce from './debounce.mjs';

import {
	pointerdown,
	pointerup/*,
	pointermove,
	pointercancel*/
} from './pointer-event-names.mjs';

const setupToggle = (el, toggleFn) => {
	let state = false;

	const onPointerDownOutside = e => {
		if (!el.contains(e.target)) {
			document.removeEventListener(pointerdown, onPointerDownOutside);
			state = false;
			toggleFn(state, el);
		}
	};

	const onPointerUp = e => {
		if (isFinite(e.button) && 0 !== e.button) {
			return;
		}

		if (true !== state) {
			document.addEventListener(pointerdown, onPointerDownOutside);
			state = true;
			toggleFn(state, el);
		}
	};

	el.addEventListener(pointerup, onPointerUp);
};

const edit = (el, options = {}) => {
	const emitter = new EventEmitter();

	if (true === options.rich) {
		let editor = null;
		const origClassName = el.className;

		let innerHTML = el.innerHTML;
		const onInput = debounce(() => emitter.emit('change', innerHTML, el), 500);

		setupToggle(el, (state, el) => {
			if (state) {
				el.classList.add('edited');

				editor = new Quill(el, { // eslint-disable-line no-undef
					modules: {
						toolbar: [
							['bold', 'italic', 'underline'],
							['link'],
							[{ list: 'ordered' }, { list: 'bullet' }]
						]
					},
					theme: 'bubble'
				});

				editor.on('text-change', () => {
					innerHTML = editor.root.innerHTML;
					onInput();
				});
			} else {
				editor.enable(false);
				editor = null;

				el.className = origClassName;
				el.innerHTML = innerHTML;
			}
		});
	} else {
		const onInput = debounce(() => emitter.emit('change', el.innerHTML, el), 500);

		setupToggle(el, (state, el) => {
			if (state) {
				el.classList.add('edited');
				el.contentEditable = 'true';
				el.addEventListener('input', onInput);
			} else {
				el.removeEventListener('input', onInput);
				el.contentEditable = 'false';
				el.classList.remove('edited');
			}
		});
	}

	return emitter;
};

export { setupToggle, edit };
