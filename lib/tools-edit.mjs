import EventEmitter from './events.mjs';

import { nanoid } from './nanoid.mjs';

import lerp from './lerp.mjs';
import clamp from './clamp.mjs';

const copySvg = srcSvg => {
	const idRpl = new Map();
	const src = srcSvg.outerHTML.
		replace(/url\s*\(\s*#([^\)\s]+)\s*\)/gi, (_, id) => {
			const newId = nanoid();
			idRpl.set(id, newId);
			return `url(#${newId})`;
		});
	const svgFrag = document.createRange().createContextualFragment(src);
	const svg = svgFrag.querySelector('svg');
	for (const el of svg.querySelectorAll('*[id]')) {
		const rplId = idRpl.get(el.id);
		if (undefined !== rplId) {
			el.id = rplId;
		}
	}
	svg.removeAttribute('id');
	return svg;
};

const parseCSSAngle = cssAngle => {
	let res = cssAngle;

	if ('string' === typeof res) {
		const cssalc = res.toLowerCase();
		res = parseFloat(cssalc);

		if (isFinite(res)) {
			if (cssalc.includes('deg')) {
				res *= Math.PI / 180;
			} else if (cssalc.includes('grad')) {
				res *= Math.PI / 200;
			} else if (cssalc.includes('turn')) {
				res *= Math.PI * 2;
			} else if (!cssalc.includes('rad')) {
				throw new Error(`Unknown css angle unit "${cssAngle.replace(/^[^\D.]+/, '')}"`);
			}
		}
	} else if (0 !== res) {
		res = NaN;
	}

	if (isNaN(res)) {
		throw new Error(`Invalid css angle value "${cssAngle}"`);
	}

	return res;
};

class Gimbal extends EventEmitter {
	constructor(
		field,
		scale = 1,
		baseToolSize = 70
	) {
		super();

		const circle = document.createElement('div');
		circle.className = 'gimbal';
		const circleSvg = copySvg(document.querySelector('.js-circle-ico'));
		const circleSvgCircle = circleSvg.querySelector('circle');
		circle.append(circleSvg);

		const rotateHandle = document.createElement('i');
		rotateHandle.className = 'handle rotate';

		const scaleHandle = document.createElement('i');
		scaleHandle.className = 'handle scale';

		const flipHHandle = document.createElement('i');
		flipHHandle.className = 'handle flip h';
		const flipHSvg = copySvg(document.querySelector('.js-arrows-ico'));
		flipHHandle.append(flipHSvg);

		const flipVHandle = document.createElement('i');
		flipVHandle.className = 'handle flip v';
		const flipМSvg = copySvg(document.querySelector('.js-arrows-ico'));
		flipVHandle.append(flipМSvg);

		circle.append(rotateHandle, scaleHandle, flipHHandle, flipVHandle);

		this._field = field;
		this._scale = scale;
		this._baseToolSize = baseToolSize;

		this._circle = circle;
		this._circleSvg = circleSvg;
		this._circleSvgCircle = circleSvgCircle;
		this._rotateHandle = rotateHandle;
		this._scaleHandle = scaleHandle;
		this._flipHHandle = flipHHandle;
		this._flipVHandle = flipVHandle;

		this._gimbScale = scale;
		this._rotation = 0;

		this._flipH = false;
		this._flipV = false;

		this._fieldRect = field.getBoundingClientRect();

		this._setupEvents();
	}

	_setupMove() {
		const circle = this._circle;
		const fieldRect = this._fieldRect;

		let allocItem;

		let startX;
		let startY;

		let startL;
		let startT;

		const onMouseUp = () => {
			document.removeEventListener('mousemove', onMouseMove);
			document.removeEventListener('mouseup', onMouseUp);
		};

		const onMouseMove = e => {
			e.preventDefault();

			const snap = true === e.shiftKey;

			const deltaPxX = e.clientX - startX;
			const deltaPxY = e.clientY - startY;

			const deltaX = deltaPxX / fieldRect.width;
			const deltaY = deltaPxY / fieldRect.height;

			let left = clamp(
				startL + deltaPxX,
				fieldRect.left, fieldRect.left + fieldRect.width
			);

			let top = clamp(
				startT + deltaPxY,
				fieldRect.top, fieldRect.top + fieldRect.height
			);

			let posX = clamp(
				lerp.t(
					fieldRect.left, fieldRect.left + fieldRect.width,
					left
				),
				0, 1
			);

			let posY = clamp(
				lerp.t(
					fieldRect.top, fieldRect.top + fieldRect.height,
					top
				),
				0, 1
			);

			if (snap) {
				posX = Math.round(posX * 30) / 30;
				posY = Math.round(posY * 50) / 50;

				left = fieldRect.left + fieldRect.width * posX;
				top = fieldRect.top + fieldRect.height * posY;
			}

			this._top = top;
			this._left = left;

			circle.style.left = `${left}px`;
			circle.style.top = `${top}px`;

			if (undefined !== allocItem) {
				allocItem.posX = posX;
				allocItem.posY = posY;
			}

			this.emit('move', posX, posY, top, left, deltaX, deltaY, deltaPxX, deltaPxY);
			this.emit('change', 'move', posX, posY);
		};

		const onMouseDown = e => {
			e.preventDefault();

			allocItem = this._allocItem;

			startX = e.clientX;
			startY = e.clientY;

			startL = this._left;
			startT = this._top;

			document.addEventListener('mousemove', onMouseMove);
			document.addEventListener('mouseup', onMouseUp);

			this.emit('startmove');
			this.emit('start', 'move');
		};

		circle.addEventListener('mousedown', onMouseDown);
	}

	_setupRotate() {
		const rotateHandle = this._rotateHandle;

		let allocItem;

		let startRotation;

		let centerX;
		let centerY;

		const onMouseUp = () => {
			document.removeEventListener('mousemove', onMouseMove);
			document.removeEventListener('mouseup', onMouseUp);
		};

		const onMouseMove = e => {
			e.preventDefault();

			const snap = true === e.shiftKey;

			const vx = e.clientX - centerX;
			const vy = e.clientY - centerY;

			let rotation = Math.atan2(vy, vx);
			let delta = rotation - startRotation;

			if (snap) {
				let turns = rotation / (Math.PI * 2) % 1;
				turns = Math.round(turns * 16) / 16;
				rotation = turns * Math.PI * 2;
				delta = rotation - startRotation;
			}

			this._rotation = rotation;

			this._updateHandlesRotation(rotation);

			if (undefined !== allocItem) {
				allocItem.rotate = `${rotation * 180 / Math.PI}deg`;
			}

			this.emit('rotate', rotation, delta);
			this.emit('change', 'rotate', rotation);
		};

		const onMouseDown = e => {
			e.preventDefault();
			e.stopPropagation();

			allocItem = this._allocItem;

			startRotation = this._rotation;

			centerX = this._left;
			centerY = this._top;

			document.addEventListener('mousemove', onMouseMove);
			document.addEventListener('mouseup', onMouseUp);

			this.emit('startrotate');
			this.emit('start', 'rotate');
		};

		rotateHandle.addEventListener('mousedown', onMouseDown);
	}

	_setupScale() {
		const scaleHandle = this._scaleHandle;

		let allocItem;

		let startSize;
		let startScale;
		let startRadius;

		let centerX;
		let centerY;

		const onMouseUp = () => {
			document.removeEventListener('mousemove', onMouseMove);
			document.removeEventListener('mouseup', onMouseUp);
		};

		const onMouseMove = e => {
			e.preventDefault();

			const dx = e.clientX - centerX;
			const dy = e.clientY - centerY;
			const radius = Math.sqrt(dx * dx + dy * dy);

			const rscale = radius / startRadius;
			const size = startSize * rscale;

			const tscale = startScale * rscale;
			this._gimbScale = tscale;
			this._size = size;

			this._updateSize();
			this._updateHandlesRotation(this._rotation);

			if (undefined !== allocItem) {
				const flipH = this._flipH;
				const flipV = this._flipV;

				const scale = tscale / this._scale;
				if (flipH || flipV) {
					delete allocItem.scale;
					allocItem.scaleX = flipH ? -scale : scale;
					allocItem.scaleY = flipV ? -scale : scale;
				} else {
					delete allocItem.scaleX;
					delete allocItem.scaleY;
					allocItem.scale = scale;
				}
			}

			this.emit('scale', tscale);
			this.emit('change', 'scale', tscale);
		};

		const onMouseDown = e => {
			e.preventDefault();
			e.stopPropagation();

			allocItem = this._allocItem;

			startSize = this._size;
			startScale = this._gimbScale;

			centerX = this._left;
			centerY = this._top;

			const dx = e.clientX - centerX;
			const dy = e.clientY - centerY;
			startRadius = Math.sqrt(dx * dx + dy * dy);

			document.addEventListener('mousemove', onMouseMove);
			document.addEventListener('mouseup', onMouseUp);

			this.emit('startscale');
			this.emit('start', 'scale');
		};

		scaleHandle.addEventListener('mousedown', onMouseDown);
	}

	_setupFlip(o9n) {
		const handle = {
			h: this._flipHHandle,
			v: this._flipVHandle
		}[o9n];

		const onClick = () => {
			const allocItem = this._allocItem;

			if ('h' === o9n) {
				this._flipH = !this._flipH;
			} else {
				this._flipV = !this._flipV;
			}

			if (undefined !== allocItem) {
				const flipH = this._flipH;
				const flipV = this._flipV;

				const scale = this._gimbScale / this._scale;
				if (flipH || flipV) {
					delete allocItem.scale;
					allocItem.scaleX = flipH ? -scale : scale;
					allocItem.scaleY = flipV ? -scale : scale;
				} else {
					delete allocItem.scaleX;
					delete allocItem.scaleY;
					allocItem.scale = scale;
				}
			}

			this.emit('flip', o9n);
			this.emit('change', 'flip', o9n);
		};

		handle.addEventListener('mousedown', onClick);
	}

	_setupEvents() {
		this._setupRotate();
		this._setupScale();
		this._setupMove();
		this._setupFlip('h');
		this._setupFlip('v');

		window.addEventListener('resize', () => {
			this.hide();
			this._fieldRect = this._field.getBoundingClientRect();
		});
	}

	_updateHandlesRotation(rotation = 0) {
		const rotateHandle = this._rotateHandle;
		const scaleHandle = this._scaleHandle;
		const flipHHandle = this._flipHHandle;
		const flipVHandle = this._flipVHandle;

		const size = this._baseToolSize * this._gimbScale;
		const radius = size / 2;

		rotateHandle.style.left = `${radius + Math.cos(rotation) * radius}px`;
		rotateHandle.style.top = `${radius + Math.sin(rotation) * radius}px`;
		rotateHandle.style.transform = `rotate(${rotation}rad)`;

		const scaleHandleRotation = Math.PI / 4 + rotation;
		scaleHandle.style.left = `${radius + Math.cos(scaleHandleRotation) * radius * Math.SQRT2}px`;
		scaleHandle.style.top = `${radius + Math.sin(scaleHandleRotation) * radius * Math.SQRT2}px`;
		scaleHandle.style.transform = `rotate(${rotation}rad)`;

		const flipHHandleRotation = Math.PI + rotation;
		flipHHandle.style.left = `${radius + Math.cos(flipHHandleRotation) * radius}px`;
		flipHHandle.style.top = `${radius + Math.sin(flipHHandleRotation) * radius}px`;
		flipHHandle.style.transform = `rotate(${rotation}rad)`;

		const flipVHandleRotation = Math.PI * 1.5 + rotation;
		flipVHandle.style.left = `${radius + Math.cos(flipVHandleRotation) * radius}px`;
		flipVHandle.style.top = `${radius + Math.sin(flipVHandleRotation) * radius}px`;
		flipVHandle.style.transform = `rotate(${rotation}rad)`;
	}

	_updateSize() {
		const circle = this._circle;
		const circleSvgCircle = this._circleSvgCircle;

		const scale = this._gimbScale;

		const top = this._top;
		const left = this._left;

		const size = this._size;
		const radius = size / 2;

		const circleStrokeWidth = 1 / size * scale;
		circleSvgCircle.setAttribute('stroke-width', circleStrokeWidth);

		circle.style.top = `${top}px`;
		circle.style.left = `${left}px`;
		circle.style.marginTop = `-${radius}px`;
		circle.style.marginLeft = `-${radius}px`;
		circle.style.width = `${size}px`;
		circle.style.height = `${size}px`;
	}

	_update() {
		const allocItem = this._allocItem;
		if (undefined !== allocItem) {
			const fieldRect = this._fieldRect;

			let flipH = false;
			let flipV = false;

			let scaleX = this._scale;
			let scaleY = scaleX;
			if (isFinite(allocItem.scale)) {
				scaleX *= allocItem.scale;
				scaleY *= allocItem.scale;
			} else {
				if (isFinite(allocItem.scaleX)) {
					scaleX *= allocItem.scaleX;
					if (scaleX < 0) { flipH = true; }
				}
				if (isFinite(allocItem.scaleY)) {
					scaleY *= allocItem.scaleY;
					if (scaleY < 0) { flipV = true; }
				}
			}

			this._flipH = flipH;
			this._flipV = flipV;

			const baseSize = this._baseToolSize;
			const scale = Math.max(Math.abs(scaleX), Math.abs(scaleY));

			this._gimbScale = scale;

			const size = baseSize * scale;
			this._size = size;

			let rotation = 0;

			if (null != allocItem.rotate) {
				rotation += parseCSSAngle(allocItem.rotate);
			}

			this._rotation = rotation;

			this._top = fieldRect.top + fieldRect.height * allocItem.posY;
			this._left = fieldRect.left + fieldRect.width * allocItem.posX;

			this._updateSize();
			this._updateHandlesRotation(rotation);
		}
	}

	_getTransform() {
		const transforms = [];

		const tscale = this._gimbScale;

		const flipH = this._flipH;
		const flipV = this._flipV;

		const scaleX = flipH ? -tscale : tscale;
		const scaleY = flipV ? -tscale : tscale;
		if (scaleX === scaleY) {
			if (1 !== scaleX) {
				transforms.push(`scale(${scaleX})`);
			}
		} else {
			transforms.push(`scale(${scaleX}, ${scaleY})`);
		}

		let rotation = this._rotation;
		if (0 !== rotation) {
			if (Math.sign(scaleX) !== Math.sign(scaleY)) {
				rotation = -rotation;
			}
			transforms.push(`rotate(${rotation * 180 / Math.PI}deg)`);
		}

		return transforms.join(' ');
	}

	setScale(scale) {
		this._scale = scale;
	}

	show(allocItem) {
		if (allocItem !== this._allocItem) {
			this._allocItem = allocItem;

			this._update();

			document.body.append(this._circle);
		}
	}

	hide() {
		if (undefined !== this._allocItem) {
			this._circle.remove();

			delete this._allocItem;
		}
	}

	bind(el) {
		let fieldRect;

		const onStart = () => {
			fieldRect = this._field.getBoundingClientRect();
		};

		const onChange = (type, ...rest) => {
			if ('move' === type) {
				const [posX, posY] = rest;
				el.style.left = `${fieldRect.left + fieldRect.width * posX}px`;
				el.style.top = `${fieldRect.top + fieldRect.height * posY}px`;
			} else if ('rotate' === type || 'scale' === type || 'flip' === type) {
				el.style.transform = this._getTransform();
			}
		};

		this.on('start', onStart);
		this.on('change', onChange);

		this._bound = { el, onStart, onChange };
	}

	unbind() {
		if (undefined !== this._bound) {
			const { onStart, onChange } = this._bound;

			this.off('start', onStart);
			this.off('change', onChange);

			delete this._bound;
		}
	}

	getCircle() {
		return this._circle;
	}
}

class ToolsEdit extends EventEmitter {
	constructor({
		toolsSrc, tools, alloc,

		toolsWrap,
		toolsCnt,
		toolHint,
		photoField
	}) {
		super();

		this._toolsSrc = toolsSrc;
		this._tools = tools;
		this._alloc = alloc;

		this._toolsWrap = toolsWrap;
		this._toolsCnt = toolsCnt;
		this._toolHint = toolHint;
		this._photoField = photoField;

		this._allocMap = new Map();

		this._namedToolsSrc = new Map(this._toolsSrc.map(e => [e.name, e]));

		this._scale = photoField.offsetHeight / 500;

		this._setupToolsSrc();
		this._reloadTools();
		this._reloadAlloc();

		this._setupEvents();
	}

	_setupEvents() {
		const alloc = this._alloc;
		const toolsCnt = this._toolsCnt;

		let selectedToolName = null;
		let selectedToolEl = null;
		let svgCopy = null;
		let allocEntry = null;
		let fieldRect;

		let startX;
		let startY;

		const onMouseUp = () => {
			selectedToolName = null;

			selectedToolEl.style.pointerEvents = '';
			selectedToolEl = null;

			if (null !== svgCopy) {
				svgCopy.remove();
				svgCopy = null;

				alloc.push(allocEntry);
				allocEntry = null;

				this._reloadAlloc();

				this.emit('change', null, this._alloc);
			}

			document.removeEventListener('mousemove', onMouseMove);
			document.removeEventListener('mouseup', onMouseUp);
		};

		const onMouseMove = e => {
			const x = e.clientX;
			const y = e.clientY;
			const dx = x - startX;
			const dy = y - startY;

			const d = Math.sqrt(dx * dx + dy * dy);

			if (null !== svgCopy || 10 < d) {
				if (null === svgCopy) {
					selectedToolEl.style.pointerEvents = 'none';

					allocEntry = {
						name: selectedToolName,
						posX: 0,
						posY: 0
					};

					svgCopy = document.createElement('div');
					svgCopy.className = 'drag-svg-cnt';
					svgCopy.style.transform = `scale(${this._scale})`;
					svgCopy.append(copySvg(selectedToolEl.querySelector('svg')));
					document.body.append(svgCopy);
				}

				allocEntry.posX = clamp(lerp.t(fieldRect.left, fieldRect.left + fieldRect.width, x), 0, 1);
				allocEntry.posY = clamp(lerp.t(fieldRect.top, fieldRect.top + fieldRect.height, y), 0, 1);

				svgCopy.style.left = `${x}px`;
				svgCopy.style.top = `${y}px`;
			}
		};

		const onMouseDown = e => {
			if (!this._toolsSrcShown) {
				for (const [name, el] of this._toolElsMap.entries()) {
					if (el.contains(e.target)) {
						selectedToolName = name;
						selectedToolEl = el;
						break;
					}
				}

				if (undefined !== selectedToolName) {
					fieldRect = this._photoField.getBoundingClientRect();

					startX = e.clientX;
					startY = e.clientY;

					document.addEventListener('mousemove', onMouseMove);
					document.addEventListener('mouseup', onMouseUp);
				}
			}
		};

		toolsCnt.addEventListener('mousedown', onMouseDown);

		document.addEventListener('mousedown', e => {
			const gimbal = this._gimbal;
			if (undefined !== gimbal) {
				const circle = gimbal.getCircle();
				if (!circle.contains(e.target)) {
					this._deselectAllocTool();
				}
			}
		});
	}

	_setupToolsSrc() {
		const toolsSrc = this._toolsSrc;
		const toolsSrcCnt = this._toolsCnt.cloneNode();
		toolsSrcCnt.classList.add('lib');

		const toolsEls = new Map();

		for (let i = 0; i < toolsSrc.length; ++i) {
			const toolEntry = toolsSrc[i];

			const el = document.createElement('i');
			el.className = 'tool-btn';
			toolsSrcCnt.append(el);

			el.addEventListener('mouseover', () => this.setHint(toolEntry.captionOverride || toolEntry.caption));
			el.addEventListener('mouseout', () => this.setHint());

			const svgSelector = toolEntry.icon_svg_selector || toolEntry.svg_selector;
			const svgSrc = document.querySelector(svgSelector);
			const svg = copySvg(svgSrc);
			for (let className of svgSelector.split(/\s+/)) {
				if (0 === className.indexOf('.')) {
					className = className.slice(1);
				}

				svg.classList.remove(className);
			}

			el.addEventListener('click', () => this.emit('selecttool', toolEntry.name));

			el.append(svg);

			toolsEls.set(toolEntry.name, el);
		}

		this._toolsEls = toolsEls;
		this._toolsSrcCnt = toolsSrcCnt;
	}

	_showTools() {
		return new Promise((resolve/*, reject*/) => {
			const toolsEls = this._toolsEls;
			const toolsCnt = this._toolsCnt;
			const toolsSrcCnt = this._toolsSrcCnt;

			const selectedToolsNames = this._tools.map(toolEntry => toolEntry.name);

			for (const [name, el] of toolsEls.entries()) {
				el.style.display = selectedToolsNames.includes(name) ? 'none' : '';
			}

			toolsCnt.replaceWith(toolsSrcCnt);

			const onMouseDownOutside = e => {
				if (!toolsSrcCnt.contains(e.target)) {
					toolsSrcCnt.replaceWith(toolsCnt);

					document.body.removeEventListener('mousedown', onMouseDownOutside);
					this.off('selecttool', onSelect);

					delete this._toolsSrcShown;

					resolve(null);
				}
			};

			document.body.addEventListener('mousedown', onMouseDownOutside);

			const onSelect = name => {
				toolsSrcCnt.replaceWith(toolsCnt);

				document.body.removeEventListener('mousedown', onMouseDownOutside);
				this.off('selecttool', onSelect);

				delete this._toolsSrcShown;

				resolve(name);
			};

			this.on('selecttool', onSelect);

			this._toolsSrcShown = true;
		});
	}

	_hideAllocActions() {
		const allocActions = this._allocActions;
		if (undefined !== allocActions) {
			allocActions.resetBtn.remove();
			allocActions.delBtn.remove();

			delete this._allocActions;
		}
	}

	_showAllocActions(allocItem) {
		this._hideAllocActions();

		const { el } = this._allocMap.get(allocItem);

		const rect = el.getBoundingClientRect();

		let top = rect.top - 42;
		if (top < 10) {
			top = rect.top + rect.height;
		}

		const resetBtn = document.createElement('i');
		resetBtn.title = 'Сбросить';
		resetBtn.className = 'action-btn reset';
		resetBtn.append(copySvg(document.querySelector('.js-reload-ico')));

		resetBtn.style.top = `${top}px`;
		resetBtn.style.left = `${rect.left + 10 + rect.width}px`;

		resetBtn.addEventListener('mousedown', e => e.stopPropagation());

		resetBtn.addEventListener('click', () => {
			this._deselectAllocTool();

			// const { item: allocItem } = this._allocMap.get(name);
			delete allocItem.scale;
			delete allocItem.scaleX;
			delete allocItem.scaleY;
			delete allocItem.rotate;

			this._reloadAlloc();

			this.emit('change', null, this._alloc);
		});

		const delBtn = document.createElement('i');
		delBtn.title = 'Удалить';
		delBtn.className = 'action-btn delete';
		delBtn.append(copySvg(document.querySelector('.js-del-ico')));

		delBtn.style.top = `${top}px`;
		delBtn.style.left = `${rect.left + 10 + rect.width + 64}px`;

		delBtn.addEventListener('mousedown', e => e.stopPropagation());

		delBtn.addEventListener('click', () => {
			this._deselectAllocTool();

			// const { item: allocItem } = this._allocMap.get(name);

			const alloc = this._alloc;
			alloc.splice(alloc.indexOf(allocItem), 1);

			this._reloadAlloc();

			this.emit('change', null, alloc);
		});

		document.body.append(resetBtn, delBtn);

		this._allocActions = { resetBtn, delBtn };
	}

	_deselectAllocTool() {
		this._hideAllocActions();

		const allocItem = this._selectedAllocItem;
		if (undefined !== allocItem) {
			const { el } = this._allocMap.get(allocItem);

			el.classList.remove('selected');

			const gimbal = this._gimbal;

			gimbal.unbind();
			gimbal.hide();

			delete this._selectedAllocItem;

			this.emit('deselect', allocItem.name);
		}
	}

	_selectAllocTool(allocItem) {
		if (allocItem !== this._selectedAllocItem) {
			this._deselectAllocTool();

			const { el } = this._allocMap.get(allocItem);
			document.body.append(el);

			let gimbal = this._gimbal;
			if (undefined === gimbal) {
				gimbal = new Gimbal(this._photoField, this._scale);

				gimbal.on('change', () => {
					this._hideAllocActions();
					this.emit('change', null, this._alloc);
				});

				this._gimbal = gimbal;
			}

			el.classList.add('selected');

			gimbal.show(allocItem);
			gimbal.bind(el);

			this._selectedAllocItem = allocItem;

			this._showAllocActions(allocItem);

			this.emit('select', allocItem.name);
		}
	}

	_clearAlloc() {
		this._deselectAllocTool();

		const allocMap = this._allocMap;
		for (const { el } of allocMap.values()) {
			el.remove();
		}

		allocMap.clear();
	}

	_createAllocEls(allocItem) {
		const el = document.createElement('div');
		el.className = 'drag-svg-cnt edit';

		el.addEventListener('click', () => this._selectAllocTool(allocItem));

		const toolEntry = this._namedToolsSrc.get(allocItem.name);
		const svg = copySvg(document.querySelector(toolEntry.svg_selector));

		el.append(svg);

		return el;
	}

	_reloadAlloc() {
		this._clearAlloc();

		const { body } = document;

		const alloc = this._alloc;
		const allocMap = this._allocMap;

		for (let i = 0; i < alloc.length; ++i) {
			const allocItem = alloc[i];
			const allocEl = this._createAllocEls(allocItem);

			allocMap.set(allocItem, { el: allocEl });

			body.append(allocEl);
		}

		this._posAllocEls();
	}

	_posAllocEls() {
		const photoField = this._photoField;
		const fldRect = photoField.getBoundingClientRect();

		for (const [item, { el }] of this._allocMap.entries()) {
			const x = fldRect.left + fldRect.width * item.posX;
			const y = fldRect.top + fldRect.height * item.posY;

			let scaleX = 1;
			let scaleY = 1;

			if (null != item) {
				if (isFinite(item.scale)) {
					scaleX = item.scale;
					scaleY = item.scale;
				} else {
					if (isFinite(item.scaleX)) { scaleX = item.scaleX; }
					if (isFinite(item.scaleY)) { scaleY = item.scaleY; }
				}
			}

			scaleX *= this._scale;
			scaleY *= this._scale;

			let transform;
			if (1 !== scaleX || 1 !== scaleY) {
				transform = `scale(${scaleX}, ${scaleY})`;
			}

			if (null != item && null != item.rotate) {
				let cssRotate = item.rotate;
				if (Math.sign(scaleX) !== Math.sign(scaleY)) {
					const rotation = -parseCSSAngle(cssRotate);
					cssRotate = `${rotation * 180 / Math.PI}deg`;
				}

				if (null != transform) {
					transform += ' ';
				} else {
					transform = '';
				}

				transform += `rotate(${cssRotate})`;
			}

			el.style.left = `${x}px`;
			el.style.top = `${y}px`;
			el.style.transform = transform;
		}
	}

	_reloadTools() {
		const tools = this._tools;
		const alloc = this._alloc;
		const toolsCnt = this._toolsCnt;

		toolsCnt.replaceChildren();

		const toolElsMap = new Map();

		for (let i = 0, sw = true; i < 12; ++i) {
			const toolEntry = tools[i];

			const el = document.createElement('i');
			el.className = 'tool-btn edit';

			if (null != toolEntry) {
				el.addEventListener('mouseover', () => this.setHint(toolEntry.captionOverride || toolEntry.caption));
				el.addEventListener('mouseout', () => this.setHint());

				const svgSelector = toolEntry.icon_svg_selector || toolEntry.svg_selector;
				const svgSrc = document.querySelector(svgSelector);
				const svg = copySvg(svgSrc);
				for (let className of svgSelector.split(/\s+/)) {
					if (0 === className.indexOf('.')) {
						className = className.slice(1);
					}

					svg.classList.remove(className);
				}

				el.dataset.name = toolEntry.name;

				const editBtn = document.createElement('i');
				editBtn.title = 'Изменить';
				editBtn.className = 'tool-sml-btn tool-edit-btn';
				editBtn.append(copySvg(document.querySelector('.js-edit-ico')));

				editBtn.addEventListener('mousedown', e => e.stopPropagation());

				editBtn.addEventListener('click', async () => {
					const toolName = await this._showTools();
					if (null != toolName && toolName !== toolEntry.name) {
						let allocChanged = false;
						for (let i = alloc.length - 1; i >= 0; --i) {
							const allocEntry = alloc[i];
							if (allocEntry.name === toolEntry.name) {
								alloc.splice(i, 1);
								allocChanged = true;
							}
						}

						let toolIdx = -1;
						for (let i = 0; i < tools.length; ++i) {
							if (tools[i].name === toolEntry.name) {
								toolIdx = i;
								break;
							}
						}

						tools.splice(toolIdx, 1, this._namedToolsSrc.get(toolName));

						this._reloadAlloc();
						this._reloadTools();

						this.emit('change', tools, allocChanged ? alloc : null);
					}
				});

				const delBtn = document.createElement('i');
				delBtn.title = 'Удалить';
				delBtn.className = 'tool-sml-btn tool-del-btn';
				delBtn.append(copySvg(document.querySelector('.js-del-ico')));

				delBtn.addEventListener('mousedown', e => e.stopPropagation());

				delBtn.addEventListener('click', () => {
					for (let i = alloc.length - 1; i >= 0; --i) {
						const allocEntry = alloc[i];
						if (allocEntry.name === toolEntry.name) {
							alloc.splice(i, 1);
						}
					}

					tools.splice(i, 1);

					this._reloadAlloc();
					this._reloadTools();

					this.emit('change', tools, alloc);
				});

				el.append(svg, editBtn, delBtn);

				toolElsMap.set(toolEntry.name, el);
			} else {
				if (true === sw) {
					el.addEventListener('mouseover', () => this.setHint('Добавить'));
					el.addEventListener('mouseout', () => this.setHint());

					el.classList.add('add');

					const svg = document.querySelector('.js-plus-sign').cloneNode(true);
					el.append(svg);

					el.addEventListener('mousedown', e => e.stopPropagation());

					el.addEventListener('click', async () => {
						const toolName = await this._showTools();
						if (null != toolName) {
							tools.push(this._namedToolsSrc.get(toolName));

							this._reloadAlloc();
							this._reloadTools();

							this.emit('change', tools, null);
						}
					});

					sw = false;
				} else {
					el.classList.add('inactive');
				}
			}

			toolsCnt.append(el);
		}

		this._toolElsMap = toolElsMap;
	}

	resize() {
		this._scale = this._photoField.offsetHeight / 500;

		const gimbal = this._gimbal;
		if (undefined !== gimbal) {
			gimbal.setScale(this._scale);
		}

		this._deselectAllocTool();
		this._posAllocEls();
	}

	setHint(value = null) {
		clearTimeout(this._clearHintTimer);

		if (null == value || 'string' === typeof value && '' === value.trim()) {
			this._clearHintTimer = setTimeout(() => { this._toolHint.textContent = ''; }, 500);
		} else {
			this._toolHint.textContent = value;
		}
	}
}

export default ToolsEdit;
