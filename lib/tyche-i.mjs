const C = -1640531527;
const D = 1367130551;

const raw = (b, c = C, d = D) => {
	let a = 0;
	/* eslint-disable space-infix-ops, semi-spacing */
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	b=b<<25^b>>>7^c;c=c-d|0;d=d<<24^d>>>8^a;a=a-b|0;b=b<<20^b>>>12^c;c=c-d|0;d=d<<16^c>>>16^a;a=a-b|0;
	/* eslint-enable space-infix-ops, semi-spacing */
	return a;
};

class TycheI {
	constructor(seed, c = C, d = D) {
		this._state = isFinite(seed) ? seed | 0 : Math.random() * 0x80000000 | 0;
		this._c = c;
		this._d = d;
	}
	getState() {
		return this._state;
	}
	setState(state) {
		this._state = state | 0;
	}
	getLinearState() {
		return (this._state >>> 0) / 0x100000000;
	}
	setLinearState(linearState) {
		this._state = (linearState * 0x100000000 | 0) << 0;
	}
	getNext() {
		this._state = raw(this._state, this._c, this._d);
		return this._state;
	}
	random() {
		return (this.getNext() >>> 0) / 0x100000000;
	}
	getRandom() {
		return () => (this.getNext() >>> 0) / 0x100000000;
	}
}

TycheI.raw = raw;

export default TycheI;
