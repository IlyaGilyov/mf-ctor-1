import {
	CLIENT_SRC_PATH,
	SRC_CSS_PATH,
	SRC_JS_PATH,

	STATIC_PATH,
	STATIC_CSS_PATH,
	STATIC_JS_PATH,

	COMPRESS_MIN_SIZE
} from '../config.mjs';

import { basename, extname, join } from 'path';
import { access, mkdir, readFile, writeFile, readdir, copyFile, stat } from 'fs/promises';
import { gzipSync, brotliCompressSync } from 'zlib';

import less from 'less';
import postcss from 'postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';

import esbuild from 'esbuild';

import minimist from 'minimist';
const argv = minimist(process.argv.slice(2));

const compressStatic = true === argv.compressstatic;
const createSourceMapsCSS = true === argv.sourcemapscss;
const createSourceMapsJS = true === argv.sourcemapsjs;
const minifyCSS = true === argv.mincss;
const minifyJS = true === argv.minjs;

const esbuildLogLevel = argv.esbloglev || 'info';


const cssEntries = [
	'login.less',
	'entry.less',
	'entry-edit.less',
	'dir-list.less',
	'entries-list.less'
];

const jsEntries = [
	'entry.js',
	'entry-edit.js',
	'dir-list.js',
	'entries-list.js'
];

const compress = async (path, data = null) => {
	if (null === data) {
		data = await readFile(path);
	}

	const gzipData = gzipSync(data);
	await writeFile(`${path}.gz`, gzipData);
	console.log(`\tCompressed file (gzip) ${path}.gz written successfully`);

	const brotliData = brotliCompressSync(data);
	await writeFile(`${path}.br`, brotliData);
	console.log(`\tCompressed file (brotli) ${path}.br written successfully`);
};

const compressIfSizeGT = async (minSize, path, data = null) => {
	if (!Buffer.isBuffer(data)) {
		data = await readFile(path);
	}

	if (minSize <= data.length) {
		await compress(path, data);
	}
};

const copyRecursive = async (srcPath, resPath, exclude = null, compressFiles = false) => {
	if (!Array.isArray(exclude)) {
		exclude = null === exclude ? [] : [exclude];
	}
	let resDirExists = false;
	const dirEnts = await readdir(srcPath, { withFileTypes: true });
	for (const dirEnt of dirEnts) {
		const { name } = dirEnt;
		if (!exclude.includes(name)) {
			const src = join(srcPath, name);
			const res = join(resPath, name);

			if (dirEnt.isDirectory()) {
				await copyRecursive(src, res, exclude, compressFiles);
			} else if (dirEnt.isFile()) {
				if (!resDirExists) {
					try {
						await access(resPath);
					} catch {
						await mkdir(resPath, { recursive: true });
					}
					resDirExists = true;
				}

				await copyFile(src, res);
				console.log(`File copied\n\tfrom ${src}\n\tto ${res}\n`);
				if (
					true === compressFiles &&
					COMPRESS_MIN_SIZE <= (await stat(res)).size
				) {
					await compress(res);
				}
			}
		}
	}
};

(async () => {
	const checkDirs = [
		STATIC_PATH,
		STATIC_CSS_PATH,
		STATIC_JS_PATH
	];

	for (const dir of checkDirs) {
		try {
			await access(dir);
		} catch {
			console.log(`Static dir "${dir}" does not exist. Creating...`);
			await mkdir(dir);
			console.log('\tok');
		}
	}

	for (const entryName of cssEntries) {
		console.log(`Processing css entry "${entryName}"...`);

		const entryPath = join(SRC_CSS_PATH, entryName);
		const entryBasename = basename(entryName, extname(entryName));
		const resPath = join(STATIC_CSS_PATH, `${entryBasename}.css`);

		const lessInput = String(await readFile(entryPath));
		const lessOutput = await less.render(lessInput, {
			paths: [SRC_CSS_PATH],
			sourceMap: createSourceMapsCSS ? { /**/ } : null
		});

		const psProc = postcss([autoprefixer]);
		if (minifyCSS) {
			psProc.use(cssnano);
		}

		const psOutput = await psProc.process(lessOutput.css, {
			from: entryPath,
			to: resPath,
			map: null != lessOutput.map ?
				{
					prev: lessOutput.map
				} :
				false
		});

		await writeFile(resPath, psOutput.css);
		console.log(`\tResult file ${resPath} written successfully`);

		if (compressStatic) {
			await compressIfSizeGT(COMPRESS_MIN_SIZE, resPath);
		}

		if (null != psOutput.map) {
			const resMapPath = `${resPath}.map`;
			await writeFile(resMapPath, String(psOutput.map));
			console.log(`\tResult file (source map) ${resMapPath} written successfully`);

			if (compressStatic) {
				await compressIfSizeGT(COMPRESS_MIN_SIZE, resMapPath);
			}
		}

		console.log(' ');
	}

	console.log('Processing JS...');

	await esbuild.build({
		entryPoints: jsEntries.map(entryName => join(SRC_JS_PATH, entryName)),
		bundle: true,
		outdir: STATIC_JS_PATH,
		sourcemap: createSourceMapsJS,
		logLevel: esbuildLogLevel,
		minify: minifyJS
	});

	if (compressStatic) {
		for (const entryName of jsEntries) {
			await compressIfSizeGT(
				COMPRESS_MIN_SIZE,
				join(STATIC_JS_PATH, entryName)
			);

			if (createSourceMapsJS) {
				await compressIfSizeGT(
					COMPRESS_MIN_SIZE,
					join(STATIC_JS_PATH, `${entryName}.map`)
				);
			}
		}
	}

	console.log(' ');

	await copyRecursive(
		CLIENT_SRC_PATH,
		STATIC_PATH,
		['css', 'js']
	);
})();
