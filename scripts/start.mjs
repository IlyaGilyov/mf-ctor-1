import {
	IS_PRODUCTION,
	ROOT,
	CLIENT_SRC_PATH,
	DATA_SRC_PATH,
	JS_LIB_PATH
} from '../config.mjs';

import os from 'os';
import { spawn } from 'child_process';
import { join } from 'path';

import debounce from '../lib/debounce.mjs';

const ASYNC_DELAY_STATIC = 300;
const ASYNC_DELAY_NODE = 100;

console.log('ENV:', IS_PRODUCTION ? 'PRODUCTION' : 'DEVELOPMENT');

if (IS_PRODUCTION) {
	if ('linux' === os.platform()) {
		// ...

		console.log('WILL BE IMPLEMENTED FOR LINUX');
	} else {
		console.error(new Error('Not implemented for current operating system'));
	}
} else {
	(async () => {
		const chokidar = await import('chokidar');

		/*
			Handle static
		*/

		const buildStatic = () => new Promise((resolve, reject) => {
			const startDate = new Date();

			console.log('\nBUILD STATIC', startDate, '\n');

			const cp = spawn(
				'node',
				[
					'build-static.mjs',
					'--sourcemapscss',
					'--sourcemapsjs'
				],
				{
					cwd: join(ROOT, 'scripts'),
					stdio: 'inherit'
				}
			);

			cp.on('error', err => reject(err));

			cp.on('close', () => {
				const endDate = new Date();
				console.log('--', endDate - startDate, endDate, '\n');

				resolve();
			});
		});

		const buildStaticDebounced = (() => {
			let promise = null;
			let timer;
			const queue = (delay = 751) => {
				if (null !== promise) {
					promise.then(() => {
						promise = null;
						queue(0);
					});
					return;
				}

				clearTimeout(timer);
				timer = setTimeout(() => {
					promise = buildStatic().catch(() => console.error(new Error('Build static failed')));
				}, delay);
			};

			return queue;
		})();

		chokidar.watch([CLIENT_SRC_PATH, JS_LIB_PATH]).on('all', () => buildStaticDebounced(ASYNC_DELAY_STATIC));

		/*
			Handle node
		*/

		let cp = null;

		const closeCurCP = () => new Promise((resolve/*, reject*/) => {
			if (null === cp) {
				resolve();
				return;
			}

			console.log('Stopping node\n');

			cp.once('close', () => resolve());

			cp.kill();
		});

		const reloadNodeDebounced = debounce(async () => {
			await closeCurCP();

			console.log('Starting node\n');

			cp = spawn(
				'node',
				[
					'index.mjs'
				],
				{
					cwd: ROOT,
					stdio: 'inherit'
				}
			);

			cp.on('close', () => {
				cp.removeAllListeners('error');
				cp = null;
				console.log('Node is stopped\n');
			});

			cp.on('error', err => console.error(err));
		}, ASYNC_DELAY_NODE);

		chokidar.watch([
			DATA_SRC_PATH,
			JS_LIB_PATH,
			join(ROOT, '.env'),
			join(ROOT, 'config.mjs'),
			join(ROOT, 'index.mjs')
		]).on('all', () => reloadNodeDebounced());
	})();
}
